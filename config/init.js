module.exports = (async () => {
	let db = require('./db/db');
	await db.sync();

	require('../plugin/SystemLogger');

	require('./db/relationship');
	require('./db/hook');

	require('./runTime');
	require('./router');

})();

