const db = require('../config/db/db');
const Sequelize = require('sequelize');


let modeConfig = {
	modelName: 'File',
	name: 'file',
	params: {
		id: {type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true},
		parentId: {type: Sequelize.UUID, comment: '所属的父Id (用于缓存标识)'},
		transition: {type: Sequelize.JSON, comment: '文件的变体属性,说明该文件属于某一个文件的处理后的文件'},

		path: {type: Sequelize.STRING, comment: '文件的详细路径(包括文件名)'},
		fileName: {type: Sequelize.STRING, comment: '文件的底层名称'},
		originalName: {type: Sequelize.STRING, comment: '文件的原始名称'},
		size: {type: Sequelize.BIGINT.UNSIGNED, comment: '文件的大小'},
		mimeType: {type: Sequelize.STRING, comment: '文件内容类型'},
		remark: {type: Sequelize.TEXT('long'), comment: '文件备注'},
		owner: {type: Sequelize.BIGINT.UNSIGNED, comment: '用于记录文件所属的用户'},
		ownerType: {type: Sequelize.INTEGER, comment: '用户的类型(详细参考define中的Session定义)'},
		publicType: {
			type: Sequelize.INTEGER,
			comment: '文件的公开类型',
			defaultValue: require('../plugin/LocalFiles').publicType.all
		},
		accessTime: {type: Sequelize.DATE, comment: '上次访问时间'},
	},
	tableConfig: {
		descName: '文件记录表',
		comment: '[非逻辑表]用于记录保存的文件',
		//默认开启 自动的createdAt,updatedAt
		timestamps: true,

		//索引
		indexes: [
			{fields: ['parentId']},
		],


	}
};


//生成model
const model = db.define(modeConfig.name, modeConfig.params, modeConfig.tableConfig);

//采用异步加载
let db_sync = model.sync().then(() => db.Models[modeConfig.modelName] = model);
db.sync(() => db_sync);

module.exports = model;
