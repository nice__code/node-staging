const start = new Date();
let init = require('../config/init');

const _ = require('lodash');
const moment = require('moment');
const crypto = require('crypto');
const db = require('../config/db/db');
const runTime = require('../config/runTime');

//加载路由
require('../config/router');


module.exports = (async () => {
	await init;
	let {SystemData} = db.Models;


	//API历史记录
	await Promise.all(runTime.api.func.map(async api => {

		let newApi = _.cloneDeep(api);
		if (newApi.handle) newApi.handle = newApi.handle.toString();
		if (newApi.path) newApi.path = newApi.path.toString();


		let history = await SystemData.findOne({
			where: {type: 'apiHistory', name: newApi.path},
			order: [['createdAt', 'desc']]
		});

		let newHistory = {type: 'apiHistory', name: newApi.path, data: {args: newApi}};
		newHistory.data.paramHash = crypto.createHash('md5').update(JSON.stringify(newApi.params || []), 'utf8').digest('hex');
		newHistory.data.apiHash = crypto.createHash('md5').update(JSON.stringify({
			...newApi,
			handle: newApi.handle.replace(/\s/g, '')
		} || {}), 'utf8').digest('hex');


		if (!history) return new SystemData(newHistory).save();

		if (newHistory.data.apiHash !== history.data.apiHash) {
			if (moment().isSame(history.createdAt, 'd')) history.destroy();
			return new SystemData(newHistory).save();
		}
	}));

	if (runTime.sessionClass === require('../plugin/Session/RedisSession')) require('../config/redis').quit();

	await db.close();
	console.log(`updateDoc 执行完毕: ${Date.now() - start}`);
})();

