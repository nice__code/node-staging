let ApiEncryption = {encrypt: {}, decipher: {}};
module.exports = ApiEncryption;

function stringToByte(str) {
	var bytes = new Array();
	var len, c;
	len = str.length;
	for (var i = 0; i < len; i++) {
		c = str.charCodeAt(i);
		if (c >= 0x010000 && c <= 0x10FFFF) {
			bytes.push(((c >> 18) & 0x07) | 0xF0);
			bytes.push(((c >> 12) & 0x3F) | 0x80);
			bytes.push(((c >> 6) & 0x3F) | 0x80);
			bytes.push((c & 0x3F) | 0x80);
		} else if (c >= 0x000800 && c <= 0x00FFFF) {
			bytes.push(((c >> 12) & 0x0F) | 0xE0);
			bytes.push(((c >> 6) & 0x3F) | 0x80);
			bytes.push((c & 0x3F) | 0x80);
		} else if (c >= 0x000080 && c <= 0x0007FF) {
			bytes.push(((c >> 6) & 0x1F) | 0xC0);
			bytes.push((c & 0x3F) | 0x80);
		} else {
			bytes.push(c & 0xFF);
		}
	}
	return bytes;
}

function Value2Bytes(str) {
	str = parseInt(str);
	str = str.toString(16);
	var pos = 0;
	var len = str.length;
	if (len % 2 != 0) {
		str = '0' + str;
	}
	len /= 2;
	var hexA = new Array();
	for (var i = 0; i < len; i++) {
		var s = str.substr(pos, 2);
		var v = parseInt(s, 16);
		hexA.push(v);
		pos += 2;
	}
	return hexA;
}

//a1加密方式
ApiEncryption.encrypt.a1 = {
	name: 'a1',
	parser: str => {
		let list = stringToByte(str);
		let ch = Date.now();
		let TimeArray = Value2Bytes(ch);

		let timeIndex1 = TimeArray[TimeArray.length - 1];
		let timeIndex2 = TimeArray[TimeArray.length - 2];
		let timeOther = TimeArray.slice(0, TimeArray.length - 2);
		let mask = timeIndex1 ^ timeIndex2;
		let random = (Math.random() * 256 | 1) % 256;
		let dataMaskSeed = (timeIndex1 + timeIndex2 + random) % 256;
		let dataMask = TimeArray.map(item => item ^ dataMaskSeed);

		let merge = [TimeArray.length ^ mask, random, timeIndex1 ^ random, timeIndex2 ^ random, ...list.map((item, index) => item ^ dataMask[index % dataMask.length]), ...timeOther.map(item => item ^ mask)];
		return String.fromCharCode(...merge);
	}
};
ApiEncryption.decipher.a1 = {
	name: 'a1',
	parser: text => {

		if (text.length < 7) throw '错误';
		let merge = [];
		for (let i = 0; i < text.length; i++) {
			merge.push(text.charCodeAt(i));
		}

		let random = merge[1];
		let timeIndex1 = merge[2] ^ random;
		let timeIndex2 = merge[3] ^ random;
		let mask = timeIndex1 ^ timeIndex2;
		let length = mask ^ merge[0];
		let time = [];
		let timestamp = 0;

		for (let i = merge.length - length + 2; i < merge.length; i++) {
			time.push(merge[i] ^ mask);
		}

		time.push(timeIndex2, timeIndex1);
		time.forEach(item => timestamp = timestamp * 256 + item);

		let dataMaskSeed = (timeIndex1 + timeIndex2 + random) % 256;
		let dataMask = time.map(item => item ^ dataMaskSeed);

		let data = merge.slice(4, merge.length - length + 2).map((item, index) => item ^ dataMask[index % dataMask.length]);
		return {data: Buffer.from(data).toString(), timestamp};
	}
};





