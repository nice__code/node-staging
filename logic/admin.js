let crypto = require('crypto');
let _ = require('lodash');


let db = require('../config/db/db');
let define = require('../config/define');
let ExpressApi = require('../plugin/ExpressApi');
let {Admin, AdminRole} = db.Models;
let logic = {};
module.exports = logic;

//密码加密函数
logic.encodePassword = function (salt, password) {
	const hmac = crypto.createHmac('md5', salt);
	hmac.update(define.adminSalt);
	let key = hmac.digest('hex');

	const passwordCipher = crypto.createCipheriv('aes256', key, define.adminIV);
	let encodePass = passwordCipher.update(`${password}`, 'utf8', 'hex');
	encodePass += passwordCipher.final('hex');

	return encodePass;
};

//密码解密函数
logic.decodePassword = function (salt, encrypted) {
	const hmac = crypto.createHmac('md5', salt);
	hmac.update(define.adminSalt);
	let key = hmac.digest('hex');

	const passwordCipher = crypto.createDecipheriv('aes256', key, define.adminIV);
	let encodePass = passwordCipher.update(encrypted, 'hex', 'utf8');
	encodePass += passwordCipher.final('utf8');

	return encodePass;
};

//从session获取db_admin
logic.pre_setAdminFromSession = async (req) => {
	await ExpressApi.timeRecord({
		nc: req.nc,
		name: 'pre_setAdminFromSession',
		func: async () => {
			let nc = req.nc;
			if (nc.session && nc.session.owner && nc.session.ownerType === define.sessionType.ADMIN)
				req.nc.db_admin = await Admin.findOne({
					where: {id: nc.session.owner},
					include: {model: AdminRole, as: 'roles', required: false}
				});
			else throw ExpressApi.Error.authError();
		}
	});

};

//验证session属于管理员
logic.pre_verifyAdminSession = async req => {
	let nc = req.nc;
	if (!nc.session || !nc.session.owner || nc.session.ownerType !== define.sessionType.ADMIN) {
		throw ExpressApi.Error.authError();
	}
};

//验证管理员是否拥有权限
logic.permissionVerifyFromReq = async (req, ...permissions) => {
	await logic.pre_setAdminFromSession(req);

	//循环验证每个权限
	for (let permission of permissions) {
		logic.permissionVerify(req.nc.db_admin, permission);
	}

};

logic.permissionVerify = (db_admin, permission) => {
	let allPermission = _.union(db_admin.permissions, ...db_admin.roles.map(role => role.permissions));

	//root权限验证

	if (allPermission.includes(define.adminPermissions.root.key)) return true;

	//基本权限验证
	if (allPermission.includes(permission.key) || permission.auth && permission.auth(allPermission)) return true;
	throw ExpressApi.Error.permissionVerifyError();
};

