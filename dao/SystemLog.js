const db = require('../config/db/db');
const Sequelize = require('sequelize');

let modeConfig = {
	modelName: 'SystemLog',
	name: 'nc_system_log',
	params: {
		id: {type: Sequelize.BIGINT.UNSIGNED, autoIncrement: true, primaryKey: true},
		type: {type: Sequelize.INTEGER, comment: 'logger的类型'},
		path: {type: Sequelize.STRING, comment: '所属API/logger的hash值'},
		realPath: {type: Sequelize.STRING, comment: '访问时,真正访问的path'},
		request: {type: Sequelize.TEXT, comment: '请求参数'},
		response: {type: Sequelize.TEXT, comment: '返回结果'},
		time: {type: Sequelize.INTEGER, comment: 'api使用时间'},
		remark: {type: Sequelize.JSON, comment: '备注'},
		ip: {type: Sequelize.STRING, comment: '访问api的ip'},
		owner: {type: Sequelize.BIGINT.UNSIGNED, comment: '所属用户'},
		ownerType: {type: Sequelize.INTEGER, comment: '所属用户的类型'},

	},
	tableConfig: {
		descName: 'api使用情况表',
		comment: '[非逻辑表] 用于记录API调用而存在的表,可用于分析错误因数, 耗时因数; 附属于 系统API LOG插件,也被System control插件依赖',
		//默认开启 自动的createdAt,updatedAt
		timestamps: true,

		//索引
		indexes: [
			{fields: ['path', 'type']},
			{fields: ['type']},
			{fields: ['ip']},
			{fields: ['owner', 'ownerType']},
			{fields: ['createdAt']},
		]
	}
};


//生成model
const model = db.define(modeConfig.name, modeConfig.params, modeConfig.tableConfig);

//采用异步加载
let db_sync = model.sync().then(() => db.Models[modeConfig.modelName] = model);
db.sync(() => db_sync);
module.exports = model;