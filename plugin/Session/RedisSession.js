let moment = require('moment');
let uuid = require('uuid');

let Session = require('./Session');
let define = require('../../config/define');
let redis = require('../../config/redis');

class RedisSession extends Session {
	// db_session = null;

	constructor({redis_session, owner, ownerType}) {
		super();
		if (redis_session) redis_session = JSON.parse(redis_session);
		this.value = redis_session && redis_session.value || {};
		this.token = redis_session && redis_session.token || uuid.v4();
		this.expiration = redis_session && new Date(redis_session.expiration);
		this.owner = this.value.owner = this.value.owner || owner;
		this.ownerType = this.value.ownerType = this.value.ownerType || ownerType;
	}

	toJSON() {
		return {
			token: this.token,
			value: this.value,
			expiration: this.expiration
		};
	}

	async save() {
		this.expiration = moment().add({seconds: define.sessionExpiration}).toDate();
		return redis.nc.setSession({session: this});
	}

	async destroy() {
		return redis.nc.deleteSession({token: this.token});
	}


	static async getById(id) {
		if (!id) return null;
		let redis_session = await redis.nc.getSession({token: id});
		if (!redis_session) return null;
		return new RedisSession({redis_session});
	}

	static async createSession({req, owner, ownerType}) {
		let session = new RedisSession({owner, ownerType});
		await session.save();
		if (req) req.nc.session = session;
		return session;

	}

	static async preprocessor(req) {

		return require('../ExpressApi').timeRecord({
			nc: req.nc,
			name: 'session-find',
			func: async () => {
				let token = req.nc.args.token;
				if (token) req.nc.session = await RedisSession.getById(token);
			}
		});
	}

	//session的后置管理器实现
	static async postprocessor(req) {
		return require('../ExpressApi').timeRecord({
			nc: req.nc,
			name: 'session-save',
			func: async () => {
				let session = req.nc.session;
				return session && session.save();
			}
		});

	}

}


module.exports = RedisSession;