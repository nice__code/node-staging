const start = new Date();
let init = require('../config/init');

const fs = require('fs-extra');
const util = require('util');
const _ = require('lodash');
const moment = require('moment');
const crypto = require('crypto');
const db = require('../config/db/db');
const define = require('../config/define');
const runTime = require('../config/runTime');
let adminLogic = require('../logic/admin');

//加载路由
require('../config/router');


fs.mkdirs(define.normalFileConversionPath).catch(() => undefined);
fs.mkdirs(define.docsPath).catch(() => undefined);
fs.mkdirs(define.tmpFilePath).catch(() => undefined);
fs.mkdirs(define.normalFilePath).catch(() => undefined);
fs.mkdirs(define.ncFilenPath).catch(() => undefined);

function randomWord(randomFlag, min, max) {
	var str = '',
		range = min,
		arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

	// 随机产生
	if (randomFlag) {
		range = Math.round(Math.random() * (max - min)) + min;
	}
	for (var i = 0; i < range; i++) {
		let pos = Math.round(Math.random() * (arr.length - 1));
		str += arr[pos];
	}
	return str;
}

module.exports = (async () => {
	await init;
	let {Admin, SystemData} = db.Models;


	//初始化ncIdFile
	if (!await fs.pathExists(define.ncIdFile)) {

		// console.log(randomWord(false,4096))
		// console.log(randomWord(false,4096).length)
		await util.promisify(fs.writeFile)(define.ncIdFile, randomWord(false, 4096));
	}

	//初始化管理员
	if (!await Admin.findOne()) {
		let db_admin = new Admin({username: 'admin'});
		db_admin.permissions.push('root');
		db_admin.password = adminLogic.encodePassword(db_admin.salt, define.adminDefaultPassword);
		await db_admin.save();
	}

	//API历史记录
	await Promise.all(runTime.api.func.map(async api => {

		let newApi = _.cloneDeep(api);
		if (newApi.handle) newApi.handle = newApi.handle.toString();
		if (newApi.path) newApi.path = newApi.path.toString();


		let history = await SystemData.findOne({
			where: {type: 'apiHistory', name: newApi.path},
			order: [['createdAt', 'desc']]
		});

		let newHistory = {type: 'apiHistory', name: newApi.path, data: {args: newApi}};
		newHistory.data.paramHash = crypto.createHash('md5').update(JSON.stringify(newApi.params || []), 'utf8').digest('hex');
		newHistory.data.apiHash = crypto.createHash('md5').update(JSON.stringify(newApi || {}), 'utf8').digest('hex');


		if (!history) return new SystemData(newHistory).save();

		if (newHistory.data.apiHash !== history.data.apiHash) {
			if (moment().isSame(history.createdAt, 'd')) history.destroy();
			return new SystemData(newHistory).save();
		}
	}));


	if (runTime.sessionClass === require('../plugin/Session/RedisSession')) require('../config/redis').quit();

	await db.close();
	console.log(`init 执行完毕: ${Date.now() - start}`);
})();

