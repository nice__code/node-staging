let db = require('../config/db/db');
let define = require('../config/define');
let runTime = require('../config/runTime');
let adminLogic = require('../logic/admin');
let ExpressApi = require('../plugin/ExpressApi');


let {Admin} = db.Models;
let {api} = runTime;
let type = ExpressApi.ParamsType;


api.add({
	name: '管理员登录',
	path: '/admin/login',
	group: 'admin',
	params: [
		{type: type.String, name: 'username', isRequest: true, comment: '用户名'},
		{type: type.String, name: 'password', isRequest: true, comment: '密码'},
	],
	secretApi: true,
	async handle(req) {
		let {username, password} = req.nc.args;

		let db_admin = await ExpressApi.timeRecord({
			nc: req.nc,
			name: 'findAdmin',
			func: () => Admin.findOne({where: {username}})
		});

		//登录信息验证
		if (!db_admin) return Promise.reject('账号或密码错误');
		if (!db_admin.enable) return Promise.reject('该账号暂时无法登陆');
		if (adminLogic.decodePassword(db_admin.salt, db_admin.password) !== password) return Promise.reject('账号或密码错误');
		db_admin.lastLoginTime = new Date();

		//todo 判断是否允许多重登录
		//if (!define.adminMultipleLogin) {
		//	let sessions = await runTime.sessionClass.find(db_admin.id, define.sessionType.ADMIN);
		//	sessions.forEach(item => item.destroy());
		//}

		//创建session
		await ExpressApi.timeRecord({
			nc: req.nc,
			name: 'sessionCreate',
			func: () => runTime.sessionClass.createSession({
				req,
				owner: db_admin.id,
				ownerType: define.sessionType.ADMIN
			})
		});
		//上次登录信息保存
		await ExpressApi.timeRecord({
			nc: req.nc,
			name: 'adminSave',
			func: () => db_admin.save()
		});

		return {
			username: db_admin.username,
			lastLoginTime: db_admin.lastLoginTime,
			permissions: db_admin.permissions,
			token: req.nc.session.token
		};
	}
});
api.add({
	name: '获取管理员列表',
	path: '/admin/list',
	group: 'admin',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '用户的sessionToken'},
		{type: type.Integer, name: 'page', isRequest: false, comment: '分页 - 页号,不填写默认获取全部'},
		{type: type.Integer, name: 'rows', isRequest: false, default: 10, comment: '分页 - 每页行数,默认为10'},
	],
	preprocessor: [
		req => adminLogic.permissionVerifyFromReq(req, define.adminPermissions.adminManager),    // 根据session判断权限
	],
	async handle(req) {
		const {page, rows} = req.nc.args;
		return ExpressApi.timeRecord({
			nc: req.nc,
			name: 'find-adminList',
			func: () => Admin.scope('normal').findAndCountAll({offset: page && (page - 1) * rows, limit: page && rows})
		});
	}
});
api.add({
	name: '添加一个管理员',
	path: '/admin/add',
	group: 'admin',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '用户的sessionToken'},
		{type: type.String, name: 'username', isRequest: true, comment: '用户名'},
		{type: type.String, name: 'password', isRequest: false, default: define.adminDefaultPassword, comment: '密码'},
		{type: type.String, name: 'realName', isRequest: false, comment: '真实姓名'},
		{type: type.String, name: 'remark', isRequest: false, comment: '备注'},
		{type: type.Array, name: 'permissions', isRequest: false, comment: '权限列表'},
	],
	preprocessor: [
		req => adminLogic.permissionVerifyFromReq(req, define.adminPermissions.adminManager),    // 根据session判断权限
	],
	async handle(req) {

		let {nc, nc: {args: {password, username}}} = req;
		let db_admin = await ExpressApi.timeRecord({
			nc,
			name: 'db_findExistAdmin',
			func: () => Admin.findOne({where: {username}})
		});
		if (db_admin) throw '已存在相同用户名的管理员';

		req.nc.args.password = adminLogic.encodePassword(password);
		return new Admin(req.nc.args).save();
	}
});
api.add({
	name: '修改一个管理员',
	path: '/admin/modify',
	group: 'admin',

	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '用户的sessionToken'},
		{type: type.Integer, name: 'id', isRequest: true, comment: '管理员的id'},
		{type: type.String, name: 'username', isRequest: false, comment: '用户名'},
		{type: type.String, name: 'password', isRequest: false, comment: '密码'},
		{type: type.String, name: 'realName', isRequest: false, comment: '真实姓名'},
		{type: type.String, name: 'remark', isRequest: false, comment: '备注'},
		{type: type.Array, name: 'permissions', isRequest: false, comment: '权限列表'},
	],
	preprocessor: [
		req => adminLogic.permissionVerifyFromReq(req, define.adminPermissions.adminManager),    // 根据session判断权限
	],
	async handle(req) {
		let {nc, nc: {args: {id, username}}} = req;
		let db_admin = await Admin.findOne({where: {id}});
		if (!db_admin) throw '不存在该管理员';

		let db_existAdmin = await ExpressApi.timeRecord({
			nc,
			name: 'db_findExistAdmin',
			func: () => Admin.findOne({where: {username, id: {$ne: id}}})
		});
		
		if (db_existAdmin) throw '已存在相同用户名的管理员';

		db_admin.set(req.nc.args);
		return db_admin.save();
	}
});
api.add({
	name: '删除一个管理员',
	path: '/admin/delete',
	group: 'admin',

	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '用户的sessionToken'},
		{type: type.Integer, name: 'id', isRequest: true, comment: '管理员的id'},
	],
	preprocessor: [
		req => adminLogic.permissionVerifyFromReq(req, define.adminPermissions.adminManager),    // 根据session判断权限
	],
	async handle(req) {
		let {id} = req.nc.args;
		let db_admin = await Admin.findOne({where: {id}});
		if (!db_admin) throw '不存在该管理员';
		return db_admin.destroy();
	}
});








