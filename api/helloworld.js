let runTime = require('../config/runTime');

let ExpressApi = require('../plugin/ExpressApi');

let {api} = runTime;

api.add({
	name: 'helloworld API',
	path: '/helloword',
	params: [
		{type: ExpressApi.ParamsType.String, name: 'token'},
		{type: ExpressApi.ParamsType.Integer, name: 'int', default: 1},
		{type: ExpressApi.ParamsType.Double, name: 'double', isRequest: false},
		{type: ExpressApi.ParamsType.String, name: 'strr'},
		{type: ExpressApi.ParamsType.Array, name: 'arr'},
		{type: ExpressApi.ParamsType.Object, name: 'obj'},
		{type: ExpressApi.ParamsType.Files, name: 'files'},
	],
	async handle(req) {
		let {files} = req.nc.args;

		await Promise.all(files.map(file => file.save()));

		return 'hello world';
	},
	test: [
		{
			name: 'helloworld测试',
			params: {
				'参数名': '参数值'
			},
			value: {code: 200, data: 'hello world'},
			async resultCheck() {
				return true;
			}
		}
	]

});

api.add({
	name: '故意错误的API - 逻辑错误',
	path: '/error',
	async handle() {
		throw '我是故意的错位';

	}
});
api.add({
	name: '故意错误的API - 系统错误',
	path: '/errorSystem',
	async handle() {
		qwe
		throw '我是故意的错位';

	}
});
api.add({
	name: '故意错误的API',

	path: '/error',
	async handle() {
		throw '我是故意的错位';
	}
});


api.add({
	name: '必须参数的API',
	path: '/isRequest',
	params: [
		{type: ExpressApi.ParamsType.Double, name: 'double', isRequest: true},
	],
	async handle(req) {
		return req.nc.args.double;
	},
	test: [
		{
			name: '正常测试',
			params: {
				double: '1.23'
			},
			value: {code: 200, data: 1.23}
		},
		{
			name: '肯定错误的测试',
			params: {
				double: '1.23'
			},
			value: {code: 200, data: 1}
		}
	]
});


api.add({
	name: '全参数API',

	path: '/params',
	params: [
		{type: ExpressApi.ParamsType.Integer, name: 'int', default: 1},
		{type: ExpressApi.ParamsType.Double, name: 'double', isRequest: false},
		{type: ExpressApi.ParamsType.String, name: 'str'},
		{type: ExpressApi.ParamsType.Array, name: 'arr'},
		{type: ExpressApi.ParamsType.Date, name: 'date'},
		{type: ExpressApi.ParamsType.Object, name: 'obj'},
		{type: ExpressApi.ParamsType.Files, name: 'files'},
		{type: ExpressApi.ParamsType.Files, name: 'files_save'},
	],
	async handle(req) {
		let {nc: {args: {files_save}}} = req;
		await Promise.all(files_save.map(item => item.save()));
	},
	test: [
		{
			name: '正在构建测试接口规范',
			params: {
				'参数名': '参数值'
			},
			value: ExpressApi.Error.customError('我是故意的错位'),
			async resultCheck() {
				return true;
			}
		}
	]
});

