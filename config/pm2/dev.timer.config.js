let package = require('../../package.json');

let maxMemory = 2048;//最大内存-单位M

module.exports = {
	apps: [
		{
			name: `${package.name}_dev-main-timer`,
			script: './timer/main.js',
			max_memory_restart: '2G',
			log_date_format: 'YYYY-MM-DD HH:mm:ss Z',
			out_file: '/dev/null',
			error_file: '/dev/null',
			node_args: ` --max-old-space-size=${maxMemory} `,
			cwd: './',
			combine_logs: true
		}
	]
};
