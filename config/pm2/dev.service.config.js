let package = require('../../package.json');
let define = require('../define');

let maxMemory = 2048;//最大内存-单位M

module.exports = {
	apps: [
		{
			name:`${package.name}_dev` ,
			script: './bin/app.js',
			log_date_format: 'YYYY-MM-DD HH:mm:ss Z',
			out_file: '/dev/null',
			error_file: '/dev/null',
			cwd: './',

			node_args: ` --max-old-space-size=${maxMemory} --inspect=0.0.0.0:${define.port+10}`,
		}
	]
};
