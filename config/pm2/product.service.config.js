let package = require('../../package.json');

let maxMemory = 2048;//最大内存-单位M

module.exports = {
	apps: [
		{
			name: package.name,
			script: './bin/app.js',
			log_date_format: 'YYYY-MM-DD HH:mm:ss Z',
			out_file: '/dev/null',
			error_file: '/dev/null',
			cwd: './',
			instances: 0,
			exec_mode: 'cluster',

			node_args: ` --max-old-space-size=${maxMemory}`,
		}
	]
};
