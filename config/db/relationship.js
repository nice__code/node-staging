const db = require('./db');

let {Admin, AdminRole, AdminAndRole} = db.Models;

Admin.hasMany(AdminAndRole, {foreignKey: 'adminId', as: 'roleRelationship'});
AdminRole.belongsToMany(Admin, {
	through: AdminAndRole,
	foreignKey: 'roleId',
	otherKey: 'adminId',
	as: 'admins'
});
Admin.belongsToMany(AdminRole, {
	through: AdminAndRole,
	foreignKey: 'adminId',
	otherKey: 'roleId',
	as: 'roles'
});