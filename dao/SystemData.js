const db = require('../config/db/db');
const Sequelize = require('sequelize');

let modeConfig = {
	modelName: 'SystemData',
	name: 'nc_system_data',
	params: {
		id: {type: Sequelize.BIGINT.UNSIGNED, autoIncrement: true, primaryKey: true},
		name: {type: Sequelize.STRING, comment: '数据名称'},
		type: {type: Sequelize.STRING, comment: '数据类型'},

		tag: {type: Sequelize.STRING, comment: '数据标签'},
		data: {
			type: Sequelize.JSON, defaultValue: () => {
			}, comment: '数据(json)',
		},


	},
	tableConfig: {
		descName: '控制工具资料表',
		comment: '[非逻辑表] 用于记录控制工具的信息',
		//默认开启 自动的createdAt,updatedAt
		timestamps: true,

		//索引
		indexes: [
			{fields: ['type', 'name']},
			{fields: ['tag']},
			{fields: ['name']},
			{fields: ['createdAt']},
		]
	}
};


//生成model
const model = db.define(modeConfig.name, modeConfig.params, modeConfig.tableConfig);

//采用异步加载
let db_sync = model.sync().then(() => db.Models[modeConfig.modelName] = model);
db.sync(() => db_sync);

module.exports = model;