import Vue from 'vue'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import VueHighlightJS from 'vue-highlightjs'

Vue.use(VueHighlightJS)
Vue.use(ElementUI);

import App from './App.vue'
import router from './router'

Vue.config.productionTip = false


import dao from './dao/dao'
import encrypt from './dao/api-Encryption-axios'

window.encrypt = encrypt;
window.vue = new Vue({
	router,
	store: dao.vuex,
	render: h => h(App)
}).$mount('#app')

