const db = require('../config/db/db');
const Sequelize = require('sequelize');

let modeConfig = {
	modelName: 'User',
	name: 'user',
	params: {
		id: {type: Sequelize.BIGINT.UNSIGNED, autoIncrement: true, primaryKey: true},
		username: {type: Sequelize.STRING, comment: '用户名'},
		password: {type: Sequelize.STRING, comment: '加密后的密码'},
		realName: {type: Sequelize.STRING, comment: '真实姓名'},
		remark: {type: Sequelize.STRING, comment: '备注'},
		lastLoginTime: {type: Sequelize.DATE, comment: '上次登录的时间'},
		permissions: {type: Sequelize.JSON, defaultValue: [], comment: '角色拥有的权限,和角色一起存在'},
		enable: {type: Sequelize.INTEGER, defaultValue: 1, comment: '是否启用 0-不启用 1-启用(默认)'},

		salt: {type: Sequelize.STRING, defaultValue: () => Math.random().toString(36).substr(2), comment: '加密用的盐,随机生成'},
	},
	tableConfig: {
		descName: '用户表',
		//默认开启 自动的createdAt,updatedAt
		timestamps: true,
		//索引
		indexes: [
			{fields: ['username']},
			{fields: ['enable']}
		],
		scopes: {
			normal: {
				attributes: ['id', 'username', 'lastLoginTime', 'permissions']
			}
		}
	}
};


//生成model
const model = db.define(modeConfig.name, modeConfig.params, modeConfig.tableConfig);

//采用异步加载
let db_sync = model.sync().then(() => db.Models[modeConfig.modelName] = model);
db.sync(() => db_sync);
module.exports = model;