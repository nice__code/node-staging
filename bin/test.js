let ava = require('ava');
let init = require('../config/init');
const runTime = require('../config/runTime');


const Test = require('../plugin/Test');

(async () => {
	await init;
	runTime.api.func.forEach(args => {
		if (!args.test) return;
		args.test.forEach((test, index) => {
			ava(`API:${args.name || args.path} - ${test.name}`, async t => {
				let result = await Test.runTest({args, index});
				let message = `return:${JSON.stringify(result.testReturn)} `;

				if (test.value !== undefined) message += ` expect:${JSON.stringify(test.value)} `;
				if (result.testCheck === false) message += ' 验证函数不通过 ';

				if (!result.pass) t.fail(message);
				else t.pass();

			});
		});
	});
})();

