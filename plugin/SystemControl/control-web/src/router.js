import Vue from 'vue'
import Router from 'vue-router'
import About from './views/About.vue'
import Home from './views/Home.vue'
import dao from './dao/dao'

let {vuex} = dao;
Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home,
			async beforeEnter(to, from, next) {

				await vuex.dispatch("updateConfig").catch(e => (vuex.commit("clearToken"), next("/login")));
				if (!vuex.state.token) return next("/login"), console.log("找不到token");
				// if (to.path === '/') return next("/api");
				return next();

			},
			children: [
				{
					path: '/api',
					name: 'api',
					component: (resolve) => resolve(require('./views/Api.vue')),
				},
				{
					path: '/model',
					name: 'model',
					component: (resolve) => resolve(require('./views/Model.vue')),
					async beforeEnter(to, from, next) {
						vuex.dispatch("updateModels");
						return next();

					},
				},
				{
					path: '/doc',
					name: 'doc',
					component: (resolve) => resolve(require('./views/Doc.vue')),
					async beforeEnter(to, from, next) {
						vuex.dispatch("updateDocList");
						return next();
					}
				},
				{
					path: '/error',
					name: 'error',
					component: (resolve) => resolve(require('./views/Error.vue')),
					async beforeEnter(to, from, next) {
						//vuex.dispatch("updateDocList");
						dao.vuex.dispatch('updateErrorNumber');
						return next();
					}
				}
			],
			redirect: "/api"
		},
		{
			path: '/about',
			name: 'about',
			component: About
		},
		{
			path: '/login',
			name: 'login',
			component: (resolve) => resolve(require('./views/Login.vue')),
			async beforeEnter(to, from, next) {
				if (dao.vuex.state.token) return next("/");
				next();

			},
		}
	]
})
