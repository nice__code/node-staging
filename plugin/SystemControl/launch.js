let express = require('express');
let path = require('path');
let util = require('util');
let fs = require('fs-extra');
let bodyParser = require('body-parser');
let compression = require('compression');
let moment = require('moment');
let _ = require('lodash');
let crypto = require('crypto');
let pinyinlite = require('pinyinlite');

let define = require('../../config/define');
let db = require('../../config/db/db');
let runTime = require('../../config/runTime');
let package = require('../../package');
let ExpressApi = require('../ExpressApi');

let systemControlApi = new ExpressApi();
let type = ExpressApi.ParamsType;
let app = express();


let {SystemLog, SystemData} = db.Models;
//gzip压缩
app.use(compression());


//路由API设置 & post参数转换
app.use(bodyParser.urlencoded({extended: true}));
app.use('/', express.static(path.join(__dirname, './display')));
app.use('/docs', express.static(define.docsPath));

app.use('/system/api', function (req, res) {
	let data = {
		undefined: []
	};
	runTime.api.func.forEach(item => {
		let title = [item.group];
		let itemData = {};
		['name', 'path', 'params', 'comment'].forEach(key => itemData[key] = item[key]);

		if (title && title[1]) {
			data[title[1]] = data[title[1]] || [];
			data[title[1]].push(itemData);
		} else {
			data['undefined'].push(itemData);
		}
	});
	let returnData = [];
	for (let key in data) {
		returnData.push({title: key, apis: data[key]});
	}
	returnData.push(returnData.shift());
	res.json(returnData);
});

app.use('/system/model', function (req, res) {
	let returnData = [];
	for (let key in db.Models) {
		let Model = db.Models[key];
		let attributes = {};
		for (let attrKey in Model.attributes) {
			attributes[attrKey] = {
				name: attrKey,
				type: Model.attributes[attrKey].type + '',
				comment: Model.attributes[attrKey].comment,
			};
		}
		returnData.push({
			name: key,
			attributes,
			descName: Model.options ? Model.options.descName : '',
			comment: Model.options && Model.options.comment
		});
	}
	res.json(returnData);
});

app.use('/system/docs', function (req, res) {
	let returnData = [];
	fs.readdirSync(define.docsPath).forEach(fileName => {
		//获取api目录下所有的文件(仅根目录),并加载
		let fileInfo = path.parse(path.resolve(define.docsPath, fileName));
		let file = fs.statSync(path.resolve(define.docsPath, fileName));
		if (file.isDirectory() || fileInfo.ext !== '.md') return;
		returnData.push(fileInfo.name);
	});
	res.json(returnData);
});
app.use('/system/apiUsed', async function (req, res) {
	let {SystemLog} = db.Models;
	let step = 0, rows = 10000;
	let finish = false;
	let returnData = {};
	let where = {};
	if (req.body.path) where.path = req.body.path;
	while (!finish) {
		let logs = await SystemLog.findAll({
			where,
			order: [['createdAt', 'asc']],
			attributes: ['id', 'path', 'time', 'responseType', 'isHandle'],
			offset: step++ * rows, limit: rows
		});
		logs.forEach(log => {
			if (!returnData[log.path]) returnData[log.path] = {
				success: 0,
				warning: 0,
				error: 0,
				successSum: 0,
				warningSum: 0,
				errorSum: 0,
				lastSuccess: '',
				lastSuccessTime: null,
				errorLog: [],
				warningLog: [],
			};
			let logData = returnData[log.path];

			if (log.responseType === ExpressApi.responseType.Success) {
				logData.success++;
				logData.successSum += log.time;
				logData.lastSuccess = log.toJSON();

			} else if (log.responseType === ExpressApi.responseType.logicError) {

				logData.warning++;
				logData.warningSum += log.time;
				if (log.isHandle === 0) {
					logData.warningLog.push(log.toJSON());
				}
			} else if (log.responseType === ExpressApi.responseType.Error) {

				logData.error++;
				logData.errorSum += log.time;
				if (log.isHandle === 0) {
					logData.errorLog.push(log.toJSON());
				}
			}


		});
		if (logs.length !== rows) finish = true;
	}

	let select = [];
	for (let info in returnData) {
		if (returnData[info].lastSuccess) select.push(returnData[info].lastSuccess);
		if (returnData[info].errorLog.length) select.push(...returnData[info].errorLog);
		if (returnData[info].warningLog.length) select.push(...returnData[info].warningLog);
	}

	let logs = (await SystemApiLog.findAll({
		where: {
			id: {
				$in: _.union(select.map(item => item.id))
			}
		},
		attributes: ['id', 'request', 'response']
	})).map(item => item.toJSON());
	let logIndex = _.indexBy(logs, 'id');
	select.forEach(item => Object.assign(item, logIndex[item.id]));


	res.json(returnData);
});
app.use('/system/apiHandle', async function (req, res) {
	let {SystemLog} = db.Models;

	let db_log = await SystemLog.findOne({where: {id: req.body.id}});
	if (!db_log) return res.json({code: 404});
	db_log.isHandle = 1;
	await db_log.save();
	return res.json({code: 200});

});


app.listen(define.SystemContralPort + 1);

let checkToken = req => {
	if (!req.nc.session) throw  ExpressApi.Error.authError();
	if (req.nc.session.ownerType !== define.sessionType.FRAME) throw  ExpressApi.Error.authError();
};

systemControlApi.add({
	name: '获取验证token',
	path: '/getAccessToken',
	params: [
		{type: type.String, name: 'id', isRequest: false, comment: '唯一id'},
		{type: type.String, name: 'password', isRequest: false, comment: '开发中心密码'},
	],
	async handle(req) {
		if (!await fs.pathExists(define.ncIdFile)) throw '验证失败';
		let {id, password} = req.nc.args;
		if (id) {
			let data = (await util.promisify(fs.readFile)(define.ncIdFile)).toString();
			if (data !== req.nc.args.id) throw '验证错误';
		} else if (password) {
			if (password !== define.apiToolPassword) throw '验证错误';
		} else throw '验证错误';


		await runTime.sessionClass.createSession({req, ownerType: define.sessionType.FRAME});
		// console.log(req.nc.session)
		return req.nc.session.token;
	}
});
systemControlApi.add({
	name: '获取全部api列表',
	path: '/api/list',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '验证后的token'},
	],

	preprocessor: [checkToken],
	async handle() {


		let list = await SystemData.findAll({
			attributes: ['name', [db.fn('MAX', db.col('createdAt')), 'updatedAt']],
			group: 'name',
			where: {type: 'apiHistory'},
			raw: true
		});

		list = _.keyBy(list, 'name');
		return runTime.api.func.map(item => {
			let api = _.pick(item, ['name', 'path', 'comment', 'group']);
			if (typeof  api.path !== 'string') api.path = api.path.toString();
			if (api.name) api.name_pinyin = pinyinlite(api.name).map(item => item.join('')).join('');


			item = _.cloneDeep(item);

			if (item.path) item.path = item.path.toString();
			if (item.handle) item.handle = item.handle.toString();

			api.paramHash = crypto.createHash('md5').update(JSON.stringify(item.params || []), 'utf8').digest('hex');
			api.apiHash = crypto.createHash('md5').update(JSON.stringify(item || {}), 'utf8').digest('hex');

			api.updatedAt = list[api.path] && list[api.path].updatedAt;

			return api;
		});
	}
});
systemControlApi.add({
	name: '获取单个API的信息',
	path: '/api/detail',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '验证后的token'},
		{type: type.String, name: 'path', isRequest: true, comment: 'api的path'},
	],

	preprocessor: [checkToken],
	async handle(req) {
		let nc = req.nc;
		let api = runTime.api.funcIndex[nc.args.path];
		if (!api) throw '找不到相关的api';
		api = _.cloneDeep(api);

		//正则路径转换
		if (api.path) api.path = api.path.toString();
		//源码信息
		if (api.handle) api.handle = api.handle.toString();

		let tasks = [];
		//访问信息
		tasks.push((async () => {
			api.successCount = 0;
			api.successTimeSummation = 0;
			let data = await db.Models.SystemLog.findAll({
				where: {path: nc.args.path},
				group: 'path',
				attributes: ['path', [db.fn('COUNT', db.col('*')), 'count'], [db.fn('sum', db.col('time')), 'time']]
			});

			if (data[0]) {
				data = data[0].toJSON();

				api.successCount = data.count;
				api.successTimeSummation = data.time;
			}

		})());
		//最后一次访问信息
		tasks.push((async () => {
			let data = await db.Models.SystemLog.findOne({
				where: {path: nc.args.path},
				order: [['createdAt', 'desc']],

			});

			if (data) api.last = data.toJSON();
		})());
		//返回demo信息
		tasks.push((async () => {
			let data = await db.Models.SystemData.findOne({
				where: {type: 'apiResponsesDemo', name: nc.args.path}
			});
			if (data) api.lastTest = data.data.demo;
		})());

		//更新时间
		tasks.push((async () => {

			let history = await SystemData.findOne({
				where: {type: 'apiHistory', name: api.path},
				order: [['createdAt', 'desc']],
				attributes: ['createdAt'],
				raw: true
			});

			if (history) api.updatedAt = moment(history.createdAt).format('YYYY-MM-DD');
		})());


		await Promise.all(tasks);

		return api;
	}
});

systemControlApi.add({
	name: '设置API的返回demo信息',
	path: '/api/setResponsesDemo',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '验证后的token'},
		{type: type.String, name: 'path', isRequest: true, comment: 'api的path'},
		{type: type.Any, name: 'data', isRequest: true, comment: 'api的demo信息'},
	],

	preprocessor: [checkToken],
	async handle(req) {
		let nc = req.nc;
		let {args: {path, data}} = nc;


		let db_demo = await SystemData.findOne({where: {type: 'apiResponsesDemo', name: path}});
		if (!db_demo) db_demo = new SystemData({type: 'apiResponsesDemo', name: path});
		if (typeof data === 'object') data = JSON.stringify(data, null, 4);
		db_demo.data = {demo: data};
		await db_demo.save();
	}
});


systemControlApi.add({
	name: '获取model数据表',
	path: '/models',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '验证后的token'},
	],

	preprocessor: [checkToken],
	async handle() {
		let returnData = [];
		//console.dir(db.Models.Admin.associations.roleRelationship.constructor.name)


		//console.log(JSON.stringify(db.Models.Admin))
		for (let key in db.Models) {
			let Model = db.Models[key];
			let attributes = {};
			//console.log(Object.keys(Model))

			for (let attrKey in Model.attributes) {
				attributes[attrKey] = {
					name: attrKey,
					type: Model.attributes[attrKey].type + '',
					comment: Model.attributes[attrKey].comment,
				};
			}
			returnData.push({
				name: key,
				attributes,
				descName: Model.options ? Model.options.descName : '',
				comment: Model.options && Model.options.comment
			});
		}
		return returnData;
	}
});

systemControlApi.add({
	name: '获取文档列表',
	path: '/doc/list',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '验证后的token'},
	],

	preprocessor: [checkToken],
	async handle() {

		let returnData = [];
		fs.readdirSync(define.docsPath).forEach(fileName => {
			//获取api目录下所有的文件(仅根目录),并加载
			let fileInfo = path.parse(path.resolve(define.docsPath, fileName));
			let file = fs.statSync(path.resolve(define.docsPath, fileName));
			if (file.isDirectory() || fileInfo.ext !== '.md') return;
			returnData.push({
				name: fileInfo.name,
				type: 'file'
			});
		});

		return returnData;
	}
});
systemControlApi.add({
	name: '获取单个文档',
	path: '/doc/get',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '验证后的token'},
		{type: type.String, name: 'type', isRequest: true, comment: '文档的类型'},
		{type: type.String, name: 'name', isRequest: true, comment: '文档的名称/id'},
	],

	preprocessor: [checkToken],
	async handle(req) {
		let {type, name} = req.nc.args;
		if (type === 'file') {
			let data = await util.promisify(fs.readFile)(path.join(define.docsPath, name + '.md'), 'utf8');
			if (typeof data !== 'string') return data.toString();
			return data;
		} else if (type === 'db') {
			throw '等待实现';
		} else throw '暂不支持的类型';
	}
});

systemControlApi.add({
	name: '获取错误列表',
	path: '/error/list',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '验证后的token'},
		{type: type.Array, name: 'type', comment: '要获取的类型'},
		{type: type.String, name: 'ip', comment: '要搜索的ip'},
		{type: type.String, name: 'path', comment: '要搜索的path'},
		{type: type.Integer, name: 'skip', comment: '分页 - 跳过skip行数据'},
		{type: type.Integer, name: 'limit', default: 10, comment: '分页 - 获取limit行数据'},

	],

	preprocessor: [checkToken],
	async handle(req) {
		let {type, ip, path, skip, limit} = req.nc.args;
		let where = {type: type && {$in: type}};
		if (ip) where.ip = ip;
		if (path) where.path = path;

		return SystemLog.findAndCountAll({
			offset: skip,
			limit: (skip || skip === 0) && limit || undefined,
			where,
			//order: [['id', 'asc']]
		});
	}
});
systemControlApi.add({
	name: '获取错误数量',
	path: '/error/number',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '验证后的token'},
	],

	preprocessor: [checkToken],
	async handle() {
		return SystemLog.count({
			where: {
				type: {
					$in: [
						define.loggerType.system,
						define.loggerType.frame,
						define.loggerType.apiSystemError,
						define.loggerType.apiParamError,
						define.loggerType.apiDecipherError,
						define.loggerType.apiOverError,
					]
				}
			}
		});
	}
});
systemControlApi.add({
	name: '删除错误信息',
	path: '/error/delete',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '验证后的token'},
		{type: type.Integer, name: 'id', comment: '错误信息的id'},
		{type: type.String, name: 'path', comment: '批量删除的path'},
		{type: type.Integer, name: 'type', comment: '批量删除的类型'},

	],

	preprocessor: [checkToken],
	async handle(req) {
		let {token, ...where} = req.nc.args;
		console.log(where)

		return SystemLog.destroy({where});
	}
});


systemControlApi.add({
	name: '获取配置',
	path: '/config',
	params: [
		{type: type.String, name: 'token', isRequest: true, comment: '验证后的token'},
	],

	preprocessor: [checkToken],
	async handle() {
		return {port: define.port, name: package.name, logType: define.loggerType};
	}
});
systemControlApi.listen({
	port: define.SystemContralPort,
	path: '/system',
	logger: null,
	staticPage: express.static(path.join(__dirname, './control-web/dist'))
});
