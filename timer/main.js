let Timer = require('cron').CronJob;
require('../config/init');



//每日 0:10 清除过期的session信息
new Timer('0 10 0 * * *', function () {
	let runTime = require('../config/runTime');

	if (runTime.sessionClass.clearExpiredSession) runTime.sessionClass.clearExpiredSession();

}, null, true, 'Asia/Chongqing');


//每天 0:20 清除api记录信息(具体保留查看配置)
new Timer('0 20 0 * * *', function () {
	let SystemLogger = require('../plugin/SystemLogger');
	SystemLogger.clearApiLog();
}, null, true, 'Asia/Chongqing');

//每天 0:30 清除过期的转化文件
new Timer('0 30 0 * * *', function () {
	let LocalFiles = require('../plugin/LocalFiles');
	LocalFiles.clearExpiredFile();
}, null, true, 'Asia/Chongqing');
