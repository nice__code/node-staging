let path = require('path');
let fs = require('fs-extra');
let sharp = require('sharp');
let moment = require('moment');
let define = require('../config/define');
let db = require('../config/db/db');

class LocalFiles {
	constructor({multerObject, DBFile}) {
		if (multerObject) this.setByMulterObejct(multerObject);
		if (DBFile) this.setByDBFile(DBFile);
	}

	setByMulterObejct({id, path, filename, mimetype, originalname, size} = {}) {
		this.id = id;
		this.path = path;
		this.fileName = filename;
		this.originalName = originalname;
		this.size = size;
		this.mimeType = mimetype;

		this.accessTime = new Date();

		this.deleteAfter = true;
	}

	setByDBFile(db_file) {
		this.id = db_file.id;
		this.path = db_file.path;
		this.fileName = db_file.fileName;
		this.originalName = db_file.originalName;
		this.size = db_file.size;
		this.mimeType = db_file.mimeType;

		this.publicType = db_file.publicType;
		this.accessTime = db_file.accessTime;
		this.parentId = db_file.parentId;

		this.transition = db_file.transition;

		this.owner = db_file.owner;
		this.ownerType = db_file.ownerType;

		return this;
	}

	async save({newPath} = {}) {
		if (this.id) return;
		if (!newPath) newPath = path.join(define.normalFilePath, this.fileName);
		await this.move(newPath);
		let db_file = new db.Models.File(this);
		await db_file.save();
		this.id = db_file.id;
		this.deleteAfter = false;
	}

	async remove() {
		if (this.id) await db.Models.File.destroy({where: {id: this.id}});
		await fs.remove(this.path);

	}

	async move(newPath) {
		let oldPath = this.path;
		newPath = path.resolve(newPath);
		await db.transaction(async () => {
			if (this.id) {
				await db.Models.File.update({path: newPath}, {where: {id: this.id}});
			}
			await fs.move(oldPath, newPath);
			this.deleteAfter = false;
			this.path = newPath;
		});


	}

	//multer信息转化为框架信息函数,作为预处理器
	static parseByMulter(req) {
		let nc = req.nc;
		let ExpressApi = require('./ExpressApi');

		let files = {};

		//构建files 对象
		(nc._multerFiles || []).forEach(item => {
			if (!files[item.fieldname]) files[item.fieldname] = [];
			let file = new LocalFiles({multerObject: item});
			if (nc.session) {
				file.owner = nc.session.owner;
				file.ownerType = nc.session.ownerType;
			}

			files[item.fieldname].push(file);
		});

		//遍历参数,从临时构建的file对象导入到 nc.args中
		(nc.config.params || []).forEach(item => {
			if (item.type !== ExpressApi.ParamsType.Files) return false;
			nc.args[item.name] = files[item.name] || [];
		});
		nc.tmpFile = files;

	}

	//后置处理器,吧没有处理过的文件删除掉
	static async postprocessor(req) {
		let nc = req.nc;

		if (!nc.tmpFile || !Object.keys(nc.tmpFile).length) return;

		return require('./ExpressApi').timeRecord({
			nc, name: 'file-postprocessor', func: () => {
				let tmpFile = nc.tmpFile;
				let tasks = [];
				for (let fileIndex in tmpFile) {
					tmpFile[fileIndex].forEach(file => file.deleteAfter && tasks.push(file.remove()));
				}
				return Promise.all(tasks);
			}
		});

	}

	static async getOneFile({fileId, transition}) {

		if (!fileId) return null;

		let db_file = null;

		//分情况,在数据库中获取文件(或其附属文件)
		if (Object.keys(transition).length) {
			//图片文件预处理
			if (transition.image) {
				let {resolution, format} = transition.image;
				let resolutionString = resolution && `${resolution.width || 'auto'}x${resolution.height || 'auto'}`;

				//查找缓存
				db_file = await db.Models.File.findOne({where: {transition, parentId: fileId}});

				//如果原来文件路径不存在文件
				if (db_file && !await fs.pathExists(db_file.path)) {
					db_file.destroy();
					db_file = null;
				}

				//查找不到缓存文件时,准备生成缓存文件
				if (!db_file) {

					//获取原始文件
					db_file = await db.Models.File.findOne({where: {id: fileId}});

					//如果原来文件路径不存在文件
					if (db_file && !await fs.pathExists(db_file.path)) {
						db_file.destroy();
						return null;
					}

					//判断是否图片文件(否则应当直接返回原文件)
					if (db_file && db_file.mimeType.includes('image/')) {
						//当符合生成条件时,准备生成新的附属文件
						let db_newFile = new db.Models.File({...db_file.toJSON(), id: undefined, createdAt: undefined});
						db_newFile.parentId = db_file.id;

						let newFileName = `${db_newFile.fileName}${resolutionString ? '_' + resolutionString : ''}${format ? '_' + format : ''}`;
						let newFilePath = `${define.normalFileConversionPath}/${newFileName}`;

						let sharpFile = sharp(db_file.path);

						//主流程处理 todo 优化处理

						//格式format-webp 处理
						if (db_file.mimeType !== 'image/webp' && format === 'webp') {
							sharpFile = sharpFile.webp();
							db_newFile.mimeType = 'image/webp';
							db_newFile.format = format;

						}
						//格式format-jpeg 处理
						if (format === 'jpeg') {
							sharpFile = sharpFile.jpeg();
							db_newFile.mimeType = 'image/jpeg';
							db_newFile.format = format;
						}
						//分辨率转化 处理
						if (resolution) {
							sharpFile = sharpFile.resize(resolution.width, resolution.height);
							db_newFile.resolution = resolutionString;
						}

						sharpFile = await sharpFile.toFile(newFilePath);


						db_newFile.path = newFilePath;
						db_newFile.fileName = newFileName;
						db_newFile.size = sharpFile.size;
						db_newFile.accessTime = new Date();
						db_newFile.transition = transition;
						db_file = await db_newFile.save();
					}
				}
			}


		} else {
			db_file = await db.Models.File.findOne({where: {id: fileId}});

			//如果原来文件路径不存在文件
			if (db_file && !await fs.pathExists(db_file.path)) {
				db_file.destroy();
				db_file = null;
			}
		}

		if (!db_file) return null;

		//记录访问日期 - 每天检索
		if (!moment().isSame(db_file.accessTime, 'day')) {
			db_file.accessTime = new Date();
			return db_file.save();
		}


		return new LocalFiles({DBFile: db_file});
	}

	static async clearExpiredFile() {
		let db_files = await db.Models.File.findAll({
			where: {
				parentId: {$ne: null},
				accessTime: {$lt: moment().subtract(define.conversionFileExpired, 's')}
			}
		});
		console.log('执行清理文件');
		return Promise.all(db_files.map(db_file => new LocalFiles({DBFile: db_file}).remove()));

	}
}

LocalFiles.publicType = {all: 0, session: 1, owner: 2, none: 3};
module.exports = LocalFiles;

