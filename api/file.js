let runTime = require('../config/runTime');
let ExpressApi = require('../plugin/ExpressApi');
let Files = require('../plugin/LocalFiles');

let type = ExpressApi.ParamsType;
let {api} = runTime;


api.add({
	name: '获取指定id的文件',
	path: /^\/ncfile/,
	params: [
		{type: type.String, name: 'token', comment: '用户的sessionToken'},

		// {type: ExpressApi.ParamsType.Integer, name: "fileId", default: 1},
		// {type: ExpressApi.ParamsType.Double, name: "resolution", isRequest: false},
	],
	group: 'file',
	noResponse: true,
	comment: 'API采用多种的',
	handle: async (req, res) => {
		let params = {transition: {}};  //参数集合
		//当有缓存标识时,直接返回缓存
		// if (req.header("If-None-Match")) return res.status(304).end();

		//解析url中的参数
		let tmpParams = req.path.split('/');
		for (let i = 1; i < tmpParams.length; i++) {
			if (tmpParams[i] === 'ncfile') {
				//基本fileId解析
				if (tmpParams[i + 1]) {
					params.fileId = tmpParams[i + 1];
					i++;
				}
			} else if (tmpParams[i] === 'w') {
				//分辨率-宽度解析 - w
				if (tmpParams[i + 1]) {
					//分辨率字符串解析
					if (!tmpParams[i + 1].match(/^[0-9]*$/)) return res.end();

					let image = params.transition.image = params.transition.image || {};
					let resolution = image.resolution = image.resolution || {};
					resolution.width = parseInt(tmpParams[i + 1]);
					if (resolution.width <= 0) return res.end();
				}
				i++;
			} else if (tmpParams[i] === 'h') {
				//分辨率-高度解析 - h
				if (tmpParams[i + 1]) {
					//分辨率字符串解析
					if (!tmpParams[i + 1].match(/^[0-9]*$/)) return res.end();

					let image = params.transition.image = params.transition.image || {};
					let resolution = image.resolution = image.resolution || {};

					resolution.height = parseInt(tmpParams[i + 1]);

					if (resolution.height <= 0) return res.end();
				}
				i++;
			} else if (tmpParams[i] === 'webp') {
				//是否自动允许webp解析
				let image = params.transition.image = params.transition.image || {};

				if (!image.format) image.format = 'webp';

			} else if (tmpParams[i] === 'jpeg') {
				let image = params.transition.image = params.transition.image || {};

				if (!image.format) image.format = 'jpeg';
			}
			else if (tmpParams[i] === 'forceResize') {
				let image = params.transition.image = params.transition.image || {};

				image.forceResize = 'true';
			}
		}


		let file = await Files.getOneFile(params);

		//当找不到文件时,直接断开连接
		if (!file) return res.status(404).end();


		//公开权限检验
		if (file.publicType === Files.publicType.all) return res.attachment(file.originalName).type(file.mimeType).sendFile(file.path);
		else if (file.publicType === Files.publicType.session && req.nc.session) return res.attachment(file.originalName).type(file.mimeType).sendFile(file.path);
		else if (file.publicType === Files.publicType.owner && req.nc.session && req.nc.session.owner === file.owner && req.nc.session.ownerType === file.ownerType) return res.attachment(file.originalName).type(file.mimeType).sendFile(file.path);

		//其他情况直接返回空
		return res.status(500).end();
	}
});
