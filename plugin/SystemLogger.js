let logger = {};
module.exports = logger;

const crypto = require('crypto');
let db = require('../config/db/db');
let define = require('../config/define');
let ExpressApi = require('./ExpressApi');

let moment = require('moment');

let paramsErrorDemo = ExpressApi.Error.paramsError();


logger.apiLog = ({req, res, remark}) => {
	let nc = res.nc;

	let message = '';
	if (nc.data && nc.data.code === 500) message = nc.data.data;
	else if (nc.data && nc.data.code !== 200) message = JSON.stringify(nc.data);

	let type = define.loggerType.normal;
	if (nc.data && nc.data.code === paramsErrorDemo.code) type = define.loggerType.apiParamError;
	else if (nc.responseType === ExpressApi.responseType.Error) type = define.loggerType.apiSystemError;
	else if (nc.responseType === ExpressApi.responseType.logicError) type = define.loggerType.apiLogicError;

	return new db.Models.SystemLog({
		type: type,
		path: nc.config && nc.config.path.toString(),
		realPath: req.path,
		request: JSON.stringify(nc.originalArgs),
		response: message,
		time: nc.apiHandleDate - nc.apiStartDate,
		remark: {...remark, timeAnalysis: nc.timeAnalysis},
		owner: nc.session && nc.session.owner,
		ownerType: nc.session && nc.session.ownerType,
		ip: req.ip && req.ip.replace('::ffff:', '')
	}).save();

};
logger.log = ({type, req, message, remark}) => {
	let nc = req && req.nc;
	let session = nc && nc.session;

	if (message instanceof Error) message = message.stack;
	if (typeof message !== 'string') message = JSON.stringify(message);

	return new db.Models.SystemLog({
		path: crypto.createHash('md5').update(message, 'utf8').digest('hex'),
		type: type,
		response: message,
		remark,

		owner: session && session.owner,
		ownerType: session && session.ownerType,
		ip: req && req.ip && req.ip.replace('::ffff:', '')

	}).save();
};

logger.debug = ({message, req, remark}) => {
	console.log(message, remark);
	logger.log({type: define.loggerType.debug, message, req, remark});
};
// 清除过期的api
logger.clearApiLog = () => {
	//非错误api保存时间
	db.Models.SystemLog.destroy({
		where: {
			createdAt: {$lt: moment().subtract(define.apiRetentionTime, 'd')},
			type: {$lt: 100}
		}
	});

	//错误api保存时间
	db.Models.SystemLog.destroy({
		where: {
			createdAt: {$lt: moment().subtract(define.apiOfErrorRetentionTime, 'd')},
			type: {$gte: 100}
		}
	});
};

process.on('uncaughtException', function (err) {
	console.error('系统异常 - 没捕获', err);
	logger.log({type: define.loggerType.system, message: err});
});
process.on('unhandledRejection', function (err) {
	console.error('系统异常 - promise没捕获', err);
	logger.log({type: define.loggerType.system, message: err});
});


