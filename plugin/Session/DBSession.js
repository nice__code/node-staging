let moment = require('moment');
let db_Session = require('../../dao/Session');

let Session = require('./Session');
let define = require('../../config/define');

class DBSession extends Session {
	// db_session = null;

	constructor({db_session, owner, ownerType}) {
		super();
		this.db_session = db_session || new db_Session();
		this.orgValue = JSON.stringify(this.db_session.value);
		this.value = this.db_session.value || {};
		this.token = this.db_session.id;
		this.owner = this.value.owner = (owner || this.value.owner);
		this.ownerType = this.value.ownerType = (ownerType || this.value.ownerType);
	}

	async save() {
		this.db_session.value = this.value;
		this.db_session.expiration = moment().add({seconds: define.sessionExpiration}).toDate();
		await this.db_session.save();
		this.orgValue = JSON.stringify(this.value);
	}

	async destroy() {
		this.db_session.destroy();
	}

	//isChange() {
	//	return this.orgValue !== JSON.stringify(this.value);
	//}

	static async getById(id) {
		if (!id) return null;
		let db_session = await db_Session.findOne({where: {id}});
		if (db_session && db_session.expiration < new Date()) {
			db_session.destroy();
			return null;
		}
		return new DBSession({db_session});
	}


	static async preprocessor(req) {

		return require('../ExpressApi').timeRecord({
			nc: req.nc,
			name: 'session-find',
			func: async () => req.nc.session = await DBSession.getById(req.nc.args.token)
		});

	}


	static async createSession({req, owner, ownerType}) {
		let session = new DBSession({owner, ownerType});
		await session.save();
		if (req) req.nc.session = session;
		return session;

	}

	//session的后置管理器实现
	static async postprocessor(req) {
		let session = req.nc.session;
		return session && session.save();
	}


	//清除所有过期的session
	static async clearExpiredSession() {
		return db_Session.destroy({
			where: {
				expiration: {
					$lt: new Date()
				}
			}
		});
	}

}


module.exports = DBSession;