import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const axios = require('axios');
const _ = require('lodash');
window.axios = axios
import createPersistedState from "vuex-persistedstate";
import encrypt from "./api-Encryption-axios";


let dao = {
	Host: '',
	APIHost: '/api',
	officialHost: `${window.location.protocol}//${window.location.hostname}`,
	api: {},
	vuex: new Vuex.Store({
		plugins: [createPersistedState({paths: ["testRecords", "token", "apiReadHistory"]})],
		state: {
			token: null, //基本的token
			apiList: [], //api列表
			models: [], //数据库模型列表
			docList: [], //文档列表
			apiDetail: {}, //api详细, path索引
			config: {}, //系统配置
			testRecords: {}, //历史测试参数, path索引
			apiReadHistory: {}, //历史测试参数, path索引
			errorNumber: 0 //待处理错误数量
		},
		mutations: {
			setToken: (state, payload) => state.token = payload.token,
			clearToken: (state) => state.token = null,
			setApiDetail: (state, detail) => Vue.set(state.apiDetail, detail.path, detail),
			setApiList: (state, payload) => state.apiList = payload.list,
			setDocList: (state, payload) => state.docList = payload.list,
			setErrorNumber: (state, number) => state.errorNumber = number,
			setConfig: (state, payload) => state.config = payload.config,
			setModels: (state, payload) => state.models = payload.models,
			setTestRecord: (state, testRecord) => Vue.set(state.testRecords, testRecord.path, testRecord),
			setReadHistory: (state, history) => Vue.set(state.apiReadHistory, history.path, history)

		},
		actions: {
			getToken: async ({commit}, {id, password}) => commit("setToken", {
				token: await dao.api.getToken({id, password})
			}),
			updateConfig: async ({commit}) => commit("setConfig", {config: await dao.api.getConfig()}),
			updateModels: async ({commit}) => commit("setModels", {models: await dao.api.getModels()}),
			updateApiList: async ({commit}) => commit("setApiList", {list: await dao.api.getApiList()}),
			updateDocList: async ({commit}) => commit("setDocList", {list: await dao.api.getDocList()}),
			updateApiDetail: async ({commit}, {path}) => commit("setApiDetail", await dao.api.getApiDetail(path)),
			updateErrorNumber: async ({commit},) => commit("setErrorNumber", await dao.api.getErrorNumber()),
		},
		getters: {
			apiListByGroup: state => _.groupBy(state.apiList, "group"),
			modelsByName: state => _.keyBy(state.models, "name"),
			loggerTypeIndex: state => state.config.logType && _.invert(state.config.logType) || {}
		}
	}),
	officialRequest: ({path, params, headers = {}}) => {
		let request = axios.post(`${dao.officialHost}:${dao.vuex.state.config.port}/api${path}`, params, {headers});
		request.then(res =>{
			let data = res.data;
			dao.api.setApiResponsesDemo({path, data})
		} );
		return request
	}
}


dao.api.getToken = async (args = {id, password}) => encrypt.runApi({
	host: dao.Host,
	path: "/getAccessToken",
	params: args
});
dao.api.getApiList = async () => encrypt.runApi({
	host: dao.Host,
	path: "/api/list",
	params: {token: dao.vuex.state.token}
});

dao.api.getApiDetail = async path => encrypt.runApi({
	host: dao.Host,
	path: "/api/detail",
	params: {token: dao.vuex.state.token, path: path}
});
dao.api.setApiResponsesDemo = async ({path, data}) => encrypt.runApi({
	host: dao.Host,
	path: "/api/setResponsesDemo",
	params: {token: dao.vuex.state.token, path, data}
});

dao.api.getConfig = async () => encrypt.runApi({
	host: dao.Host,
	path: "/config",
	params: {token: dao.vuex.state.token}
});


dao.api.getModels = async () => encrypt.runApi({
	host: dao.Host,
	path: "/models",
	params: {token: dao.vuex.state.token}
});

dao.api.getDocList = async () => encrypt.runApi({
	host: dao.Host,
	path: "/doc/list",
	params: {token: dao.vuex.state.token}
});

dao.api.getDocDetail = async ({name, type}) => encrypt.runApi({
	host: dao.Host,
	path: "/doc/get",
	params: {token: dao.vuex.state.token, name, type}
});
dao.api.getErrorList = async ({type, ip, path, skip = 0, limit = 10}) => encrypt.runApi({
	host: dao.Host,
	path: "/error/list",
	params: {token: dao.vuex.state.token, type, ip, path, skip, limit}
});
dao.api.getErrorNumber = async () => encrypt.runApi({
	host: dao.Host,
	path: "/error/number",
	params: {token: dao.vuex.state.token}
});

dao.api.getErrorDelete = async ({id, path,type}) => encrypt.runApi({
	host: dao.Host,
	path: "/error/delete",
	params: {token: dao.vuex.state.token, id, path,type}
});

window.dao = dao;


//vue混入配置
dao.vuePlugin = {
	install: Vue => Vue.mixin({
		beforeCreate() {
			this.$dao = dao;
		}
	})
}


export default dao;

