const db = require('../config/db/db');
const Sequelize = require('sequelize');

let modeConfig = {
	modelName: 'AdminRole',
	name: 'admin_role',
	params: {
		id: {type: Sequelize.BIGINT.UNSIGNED, autoIncrement: true, primaryKey: true},
		name: {type: Sequelize.STRING, comment: '权限的名字'},
		permissions: {type: Sequelize.JSON, defaultValue: [], comment: '角色拥有的权限,和角色一起存在',},

	},
	tableConfig: {
		descName: '管理员角色表',
		comment: '用于记录管理员的角色',
		//默认开启 自动的createdAt,updatedAt
		timestamps: true,

		//索引
		indexes: [
			// {fields: ['']}
		]
	}
};


//生成model
const model = db.define(modeConfig.name, modeConfig.params, modeConfig.tableConfig);

//采用异步加载
let db_sync = model.sync().then(() => db.Models[modeConfig.modelName] = model);
db.sync(() => db_sync);

module.exports = model;