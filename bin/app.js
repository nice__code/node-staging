const init = require('../config/init');

let define = require('../config/define');
let runTime = require('../config/runTime');

(async () => {
	await init;

	require('../plugin/SystemControl/launch');
	runTime.api.listen({port: define.port});
})();


