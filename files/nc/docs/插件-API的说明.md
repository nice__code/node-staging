## 插件-API
目前只有ExpressAPI,即使基于express来做的底层api位置控制

这个插件的意义是什么?


### 插件的构造
原则上,插件是个类:`ExpressApi` (类名之后更改)

#### `ExpressApi` 静态属性
| 参数名         | 类型           | 描述 |
| :-------------: |:-------------:| -----|
| ParamsType       |Object |(key:String,value:String); 参数类型的索引,在添加API时可以指定参数类型 |
| Error       |Object |(key:String,value:function); 用于生成报错信息对象的函数 的索引|
| preprocessor       |Object |内置预处理器的的对象索引,里面有"参数预处理器" |
| quick       |Object | 快捷API函数的索引 (准备废弃)|
| quickUtil       |Object | 快捷API函数的工具的索引 (准备废弃)|


#### `ExpressApi` 实例 固定属性

| 参数名         | 类型           | 描述 |
| :-------------: |:-------------:| -----|
| preprocessor       |Array |总前置预处理器,待执行的API,都会预先执行数组内的所有函数<br>采用await单个顺序执行,构造类时默认加入 "参数预处理器" |
| postprocessor       |Array | 总后置预处理器,API执行完毕之后,都会预先执行数组内的所有函数<br>采用await单个顺序执行,如果出现错误,则算API执行错误 |
| func       |Array |设置的API集合,保存的是设置API时使用的args(参数)|
| funcIndex       |Object |key为path的 func索引|
| isDebug       |bool | 设置是否debug状态-在发生内部错误的时候,是否返回API的错误信息 |
| router       |object | express的路由 |

#### `ExpressApi` 实例的内部方法
略(todo)

### 编写API的方法

#### 生成API使用的参数
略(todo)

#### API的运行时

由于API基于Express,执行handle函数的时候,大致会传入两个参数: req,res

##### 内置参数的说明
req会集成express的函数,同时会多出一个`nc`的对象,这个对象的作用是,把框架处理的信息写入到该对象中

`nc`的引用会出现在`handle`函数中的 `req`和`res`参数中

`nc`对象的属性-开发可用(可以用到的)

| 参数名         | 类型           | 描述 |
| :-------------: |:-------------:| -----|
| args       |Object |处理后的参数集合 |
| session       |Object | 用户的session,从args中的token获取|
| db_admin       |Object | 所属的管理员,当执行管理员前置的时候,会有该属性|

`nc`对象的属性-框架内置(个人不需要用到的)

| 参数名         | 类型           | 描述 |
| :-------------: |:-------------:| -----|
| config       |Object | API生成时的配置, |
| originalArgs       |Object | 处理前的参数集合,用于api_log |
| apiStartDate       |Date |API开始执行的时候的Date,用于计算耗时|
| apiHandleDate       |Date |API结束执行的时候的Date,用于计算耗时|
| data       |mix | API返回的数据,在返回结束时,会写入api_log |







