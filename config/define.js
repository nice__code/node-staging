let path = require('path');
let defined = {};
module.exports = defined;

//session的相关设置
defined.sessionExpiration = 24 * 60 * 60;        //session过期时间,单位秒
defined.sessionType = {
	ADMIN: 1,
	USER: 2,
	FRAME: 3
};

//logger相关设置
defined.apiRetentionTime = 30;						//正常访问api访问保存天数,单位天
defined.apiOfErrorRetentionTime = 30 * 3;			//错误api访问保存天数,单位天

defined.loggerType = {
	normal: 0,				//正常API
	debug: 1,				//debug信息
	apiLogicError: 90,		//API自定义逻辑错误

	//100 - 系统异常异常
	frame: 101,				//框架错误
	system: 102,			//系统没捕获的异常

	//200 - api异常访问系
	apiSystemError: 212,	//API抛出异常的错误

	apiParamError: 221,	//API抛出异常的错误

	apiDecipherError: 242, //加密函数解密失败

	apiOverError: 251, //API异常访问过多拦截
};

//管理员账号的登录,密码及其加密定义
defined.adminIV = 'nicecode-staging';            //admin加密用的im,需要16个字符
defined.adminSalt = 'QQ:444811296';             //admin密码的总体加盐
defined.adminDefaultPassword = '123456';         //默认账号密码
defined.adminMultipleLogin = true;				//todo 是否允许多重登录

//管理员权限相关设置
defined.adminPermissions = {                    //admin权限定义
	root: {
		key: 'root',
		name: '总权限',
	},
	adminManager: {
		key: 'adminManager',
		name: '总权限',

	}
};

//系统运行相关配置
defined.apiToolPassword = 'nicecode-staging';               //开发中心密码
defined.SystemContralPort = 11101;               			//控制系统的端口
defined.port = 11100;										//api接口端口

//文件&路径相关配置
defined.tmpFilePath = path.resolve(__dirname, '../files/tmp');             					//临时文件夹的路径
defined.normalFilePath = path.resolve(__dirname, '../files/normal');       					//默认保存文件的路径
defined.normalFileConversionPath = path.resolve(__dirname, '../files/conversion');       	//转化文件的保存路径

defined.ncFilenPath = path.resolve(__dirname, '../files/nc');       						//框架相关文件路径
defined.docsPath = path.resolve(defined.ncFilenPath, './docs');             				//系统文档的路径
defined.ncIdFile = path.resolve(defined.ncFilenPath, 'd');   								//id文件位置(目前ID文件暂时未起作用)

defined.conversionFileExpired = 7 * 24 * 60 * 60; 											//转化文件保留时间,单位秒

//api拦截相关设置
defined.apiInterceptForErrorLimit = true;			//是否开启  过多错误时的API拦截; (仅当在index配置了redis时生效)
defined.apiInterceptForErrorLimit_number = 60;		//过多错误时的API拦截 - 错误数量
defined.apiInterceptForErrorLimit_recordTime = 60;	//过多错误时的API拦截 - 记录时间(秒)
defined.apiInterceptForErrorLimit_banTime = 60;		//过多错误时的API拦截的时间 (秒)

//加密接口相关设置
defined.secretApiTimeLimit = 60 * 1000;					//加密接口的API的时间限制 [从构建到接受请求的不可超过的毫秒数] (毫秒)