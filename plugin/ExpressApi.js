const express = require('express');
const moment = require('moment');
const multer = require('multer');

const _ = require('lodash');
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');

const define = require('../config/define');
const conifg = require('../config/index');
let redis = require('../config/redis');

let ApiEncryption = require('./ApiEncryption');
let dataParser = bodyParser.text({type: 'nicecode/*'});


class ExpressApi {
	constructor() {
		this.preprocessor = [];
		this.postprocessor = [];
		this.func = [];

		this.funcIndex = {};
		this.isDebug = true;
		this.router = express.Router();
		this.logger = require('./SystemLogger');

		let Files = require('./LocalFiles');

		//错误访问限制处理组
		if (conifg.redis && define.apiInterceptForErrorLimit) {
			this.preprocessor.push(ExpressApi.preprocessor.overErrorIntercept);
			this.postprocessor.push(ExpressApi.preprocessor.overErrorInterceptRecord);
		}


		this.preprocessor.push(ExpressApi.preprocessor.params);	//参数解析器
		this.preprocessor.push(Files.parseByMulter);	//文件 前置解析器
		this.postprocessor.push(Files.postprocessor);	//文件 后置解析器

		//session 解析器组
		const runTime = require('../config/runTime');
		if (runTime.sessionClass && runTime.sessionClass.preprocessor) {
			this.preprocessor.push(runTime.sessionClass.preprocessor);
		}
		if (runTime.sessionClass && runTime.sessionClass.postprocessor) this.postprocessor.push(runTime.sessionClass.postprocessor);


	}

	/**
	 * 添加函数
	 * @param {Object} args {
 *      path: api路径
 *      params: [{ 必须参数集合
 *          name: 参数名字
 *          type: 参数类型
 *          default: 默认值
 *          isRequest: 是否必须的
 *      }]
 *      noResponse: 是否不自动信息返回,默认false
 *      preprocessor:
 *      handle: 处理函数
 * }
	 */
	add(args) {

		if (!args.path) return console.error('必须填写路径(path)');
		if (this.funcIndex[args.path.toString()]) return console.error('重复的访问路径:' + args.path);

		//api - 测试名字唯一检测
		if (args.test) {
			let testIndex = _.groupBy(args.test, 'name');
			for (let key in testIndex) {
				if (testIndex[key].length > 1) return console.error(`${args.name} - 重复的测试名:${key}`);
			}
		}

		//自动添加的前置管理器
		if (!args.preprocessor) args.preprocessor = [];
		if (!(args.preprocessor instanceof Array)) return console.error('前置处理函数列表(preprocessor)必须为数组');

		//自动添加的后置管理器
		if (!args.postprocessor) args.postprocessor = [];

		//handler函数验证
		if (!args.handle || !(args.handle instanceof Function)) return console.error('处理函数(handle)必须为函数');


		//把api信息记录到func中,以便生成资料
		this.func.push(this.funcIndex[args.path.toString()] = args);

		let expressPlugin = [];

		(args.params || []).filter(item => item.type === ExpressApi.ParamsType.Files).length && expressPlugin.push(multer({dest: define.tmpFilePath}).any());


		this.router.route(args.path)
			.get(expressPlugin, (req, res) => this._httpHandler({req, res, args}))
			.post(expressPlugin, (req, res) => this._httpHandler({req, res, args}));

	}


	//根据请求,获取API调用后的结果 todo 逻辑优化
	async _getResult(req, res) {
		let nc = req.nc;
		let args = nc.config;
		try {
			if (!args) throw ExpressApi.Error.undefinedError();

			//总前置函数运行处理
			for (let func of this.preprocessor) {
				await func(req, res);
			}

			//前置函数运行处理
			for (let func of args.preprocessor) {
				await func(req, res);
			}


			//处理逻辑函数
			let data = null;
			try {
				data = await args.handle(req, res);
				data = nc.data = {code: 200, data};
				nc.responseType = ExpressApi.responseType.Success;

			} catch (error) {
				//如果发生的是内部错误
				if (error instanceof Error) {
					nc.responseType = ExpressApi.responseType.Error;
					console.error('内部发生错误', error);
					nc.data = ExpressApi.Error.systemError(error.stack);
					data = this.isDebug ? nc.data : ExpressApi.Error.systemError();
				} else {
					//如果发生的是自定义错误
					nc.responseType = ExpressApi.responseType.logicError;
					if (typeof error === 'object') nc.data = Object.assign(ExpressApi.Error.customError(), error);
					else data = nc.data = ExpressApi.Error.customError(error);
				}
			}

			//后置函数运行处理
			for (let func of args.postprocessor) {
				await func(req, res, data);
			}

			//总后置函数运行处理
			for (let func of this.postprocessor) {
				await func(req, res, data);
			}

			//返回整段数据
			return data;

		} catch (error) {

			// // 错误处理
			// if (args.noResponse) return

			//如果发生的是内部错误
			if (error instanceof Error) {
				nc.responseType = ExpressApi.responseType.Error;
				console.error('内部发生错误', error);
				nc.data = ExpressApi.Error.systemError(error.stack);
				return this.isDebug ? nc.data : ExpressApi.Error.systemError();
			}

			//如果发生的是自定义错误
			nc.responseType = ExpressApi.responseType.logicError;
			if (typeof error === 'object') return nc.data = Object.assign(ExpressApi.Error.customError(), error);
			return nc.data = ExpressApi.Error.customError(error);
		}
	}

	//根据请求,生成完整的nc Object
	async _getNCObject({req, res, args}) {
		let nc = {};            // 总前置属性
		nc.apiStartDate = new Date();    //处理用时
		nc.originalArgs = Object.assign({}, req.params, req.query, req.body);
		nc.timeAnalysis = {}; //时间分析属性


		if (req.files) nc._multerFiles = req.files; //处理预备解析的文件
		if (res.end) res.end = new Proxy(res.end, {
			apply: (target, thisBinding, args) => {
				req.nc.apiHandleDate = new Date();
				if (this.logger) this.logger.apiLog({req: req, res: res});

				return target.call(thisBinding, ...args);
			}
		}); //end 自动记录
		if (args) nc.config = _.cloneDeep(args);
		return nc;

	}

	//用于处理http请求的处理器
	async _httpHandler({req, res, args}) {
		req.nc = res.nc = await this._getNCObject({req, res, args});
		let data = await this._getResult(req, res);

		if (data.code !== 200) res.jsonp(data);
		else if (args && !args.noResponse) res.jsonp(data);
	}

	async _secretHandler(req, res, next) {

		await new Promise(resolve => dataParser(req, res, resolve));
		//自适应解密函数
		let decipher = ApiEncryption.decipher[req.get('Content-Type').split('/')[1]];
		if (!decipher) {
			if (this.logger) this.logger.log({
				type: define.loggerType.apiDecipherError,
				message: `找不到加密方法:${req.get('Content-Type')}`,
				req,
			});
			return next(`找不到解密方式:${req.get('Content-Type')}`);

		}

		// 解密错误处理
		let plaintext = null;
		try {
			plaintext = decipher.parser(req.body);
			plaintext.data = JSON.parse(plaintext.data);
		} catch (e) {
			if (this.logger) this.logger.log({
				type: define.loggerType.apiDecipherError,
				message: plaintext,
				req
			});
			return next('解密异常');
		}


		let request = plaintext.data;
		//timestamp 时间限制
		let timeDiffer = Date.now() - request.timestamp;
		if (timeDiffer > define.secretApiTimeLimit || timeDiffer < 0) {
			if (this.logger) this.logger.log({
				type: define.loggerType.apiDecipherError,
				message: '时间戳异常',
				req,
				remark: {
					timestamp: request.timestamp,
				}
			});
			return next('时间戳异常');
		}

		//构建 预备执行参数: args & 参数插入req
		let args = this.funcIndex[request.path];
		req.body = request.params || {};

		//todo 参数检测
		this._httpHandler({req, res, args});
	}

	//监听端口
	async listen({port, logger = require('./SystemLogger'), path = '/api', staticPage}) {
		let app = express();

		//gzip压缩
		app.use(compression());
		//跨域设置
		app.use(cors());

		if (staticPage) app.use('/', staticPage);

		//路由API设置 & post参数转换+
		app.use(bodyParser.json());
		app.use(bodyParser.urlencoded({extended: true}));
		app.use(path, this.router);
		app.post('/secret', this._secretHandler.bind(this));

		//捕获没定义的函数
		app.use((req, res) => this._httpHandler({req, res}));

		app.use(ExpressApi.errorHandle);
		app.listen(port);
		this.logger = logger;
		console.log(`已监听端口:${port}`);


	}

	//执行某个执行某个API
	async runApi({path, params, req = {}, res = {}}) {
		let args = this.funcIndex[path];
		if (!args) throw '不存在该api!';

		if (params) req.params = params;

		req.nc = res.nc = await this._getNCObject({req, res, args});
		return this._getResult(req, res);
	}

	//错误处理的函数
	static errorHandle(err, req, res, next) {
		if (err instanceof Error) {
			console.error('系统框架发生异常', err);

			require('./SystemLogger').log({type: define.loggerType.frame, message: err, ip: req.ip});
			return res.jsonp(ExpressApi.Error.systemError('系统框架发生异常:' + err.message));
		}

		//未定义api处理
		return res.jsonp(ExpressApi.Error.undefinedError());
	}

	//耗时记录函数
	static async timeRecord({nc, name, func}) {
		let start = Date.now();
		let data = await func();
		nc.timeAnalysis[name] = Date.now() - start;
		return data;
	}
}


ExpressApi.describe = {
	name: 'api的名字',
	path: 'api的路径',
	params: [
		{
			type: '参数的类型',
			name: '参数的名字',
			isRequest: '参数是否必须 (bool)',
			comment: '参数的描述',
			default: '参数的默认值',
			isGlobal: '快速生成api-是否全部都有  (bool)',
			isSearch: '快速生成api-是否搜索的项  (bool)',
			isGet: '快速生成api-是否获取的项  (bool)',
			isAdd: '快速生成api-是否添加的项  (bool)',
			isModify: '快速生成api-是否修改的项  (bool)',
			isModifyKey: '快速生成api-修改项的搜索key  (bool)',
			isDelete: '快速生成api-是否删除的项  (bool)',

			mix: '快速生成api- 混合搜索的字段',
			mixPrefix: '快速生成api- 混合搜索的前缀'
		}
	],
	preprocessor: '前置判断函数 ([function])',
	handle: '真正的逻辑处理函数 (function)',
};

ExpressApi.ParamsType = {
	Integer: 'Integer',
	Double: 'Double',
	String: 'String',
	Array: 'Array',
	Object: 'Object',
	Date: 'Date',
	Files: 'Files',
	Any: 'Any',
};
ExpressApi.responseType = {
	Success: 0,
	Error: 1,
	logicError: 2
};
ExpressApi.Error = {
	paramsError(data) {
		return {code: 400, message: '参数不正确', data};
	},
	undefinedError() {
		return {code: 404, message: '找不到该API'};
	},
	systemError(data) {
		return {code: 500, data};
	},
	customError(message) {
		return {code: 503, message};
	},
	authError() {
		return {code: 510, message: '请先登录'};
	},
	permissionVerifyError() {
		return {code: 511, message: '该角色没有权限'};
	}

};
ExpressApi.preprocessor = {
	params: (req) => {
		let nc = req.nc;
		nc.args = {};

		let params = req.nc.config.params;
		if (!params || params.length === 0) return;

		return Promise.all(params.map(param => {

			let arg = nc.originalArgs[param.name];
			//非空判断 与 默认值处理
			if (arg === undefined) {
				if (param.default) arg = param.default;
				else if (param.isRequest) throw ExpressApi.Error.paramsError(param.name);
				else return;
			}
			if (arg === '' && param.type !== ExpressApi.ParamsType.String) arg = null;

			//类型判断
			if (arg === null) nc.args[param.name] = arg;

			else if (param.type === ExpressApi.ParamsType.Integer) {
				//判断是否整数
				arg = parseFloat(arg);
				if (!Number.isInteger(arg)) throw ExpressApi.Error.paramsError(param.name);
			}
			else if (param.type === ExpressApi.ParamsType.Double) {
				//判断是否小数
				arg = parseFloat(arg);
				if (Number.isNaN(arg)) throw ExpressApi.Error.paramsError(param.name);
			}
			else if (param.type === ExpressApi.ParamsType.String) {
				//判断是否字符串
				if (typeof arg !== 'string') throw ExpressApi.Error.paramsError(param.name);
			}
			else if (param.type === ExpressApi.ParamsType.Array) {
				//判断判断是否数组
				if (!(arg instanceof Array)) {
					try {
						arg = JSON.parse(arg);
						if (!(arg instanceof Array)) throw 'error';
					} catch (e) {
						throw ExpressApi.Error.paramsError(param.name);
					}
				}
			}
			else if (param.type === ExpressApi.ParamsType.Object) {
				//判断判断是否数组
				try {
					arg = JSON.parse(arg);
				} catch (e) {
					throw ExpressApi.Error.paramsError(param.name);
				}
			}
			else if (param.type === ExpressApi.ParamsType.Date) {
				try {
					if (!isNaN(arg) && Number.isInteger(parseFloat(arg))) arg = parseInt(arg);
					let time = moment(arg);
					if (!time.isValid()) throw 'error';
					arg = time.toDate();
				} catch (e) {
					throw ExpressApi.Error.paramsError(param.name);
				}
			}

			nc.args[param.name] = arg;


		}));


	},
	overErrorIntercept: async (req) => {
		return ExpressApi.timeRecord({
			nc: req.nc,
			name: 'pre_overErrorIntercept',
			func: async () => {
				let ip = req.ip && req.ip.replace('::ffff:', '');
				let isBan = await redis.nc.isIpBan({ip});
				if (isBan) throw '本ip访问过于频繁,请稍后再访问';
			}
		});
	},
	overErrorInterceptRecord: async (req, res, data) => {
		if (data.code !== 200) {
			let ip = req.ip && req.ip.replace('::ffff:', '');
			redis.nc.apiErrorCountIncrease({ip}).then(data => {
				if (data === define.apiInterceptForErrorLimit_number) require('./SystemLogger').log({
					type: define.loggerType.apiOverError,
					message: `错误次数过多,禁止运行${define.apiInterceptForErrorLimit_banTime}秒`,
					ip: req.ip
				});
			});

		}
	},
};
ExpressApi.quick = {
	list({name, path, preprocessor, Model, params, isPaging = true}) {
		let actualParams = JSON.parse(JSON.stringify(params.filter(item => item.isSearch || item.isGlobal)));
		actualParams.forEach(item => item.isRequest = item.isGlobal && item.isRequest);

		if (isPaging) {
			actualParams.push({
				type: ExpressApi.ParamsType.Integer,
				name: 'page',
				isRequest: false,
				comment: '分页 - 页号,不填写默认获取全部'
			});
			actualParams.push({
				type: ExpressApi.ParamsType.Integer,
				name: 'rows',
				isRequest: false,
				default: 10,
				comment: '分页 - 每页行数,默认为10'
			});
		}
		runTime.api.add({
			name, path, preprocessor,
			params: actualParams,
			async handle(req) {
				const {page, rows} = req.nc.args;
				let where = {};
				req.nc.config.params.filter(item => item.isSearch).forEach(param => {
					if (req.nc.args[param.name] === undefined) return;
					if (param.mix || param.mixPrefix) {
						param.mix = param.mix || param.name;

						let mix = where[param.mix] || {};
						if (param.mixValue) param.mixValue = eval(param.mixValue);
						mix[param.mixPrefix] = param.mixValue ? param.mixValue(req.nc.args[param.name]) : req.nc.args[param.name];
						console.log(mix, param.mixValue, param.mixValue && param.mixValue(req.nc.args[param.name]));
						return where[param.mix] = mix;
					}
					where[param.name] = req.nc.args[param.name];
				});

				return Model.findAndCountAll({where, offset: page && (page - 1) * rows, limit: page && rows});
			}
		});
	},
	get({name, path, preprocessor, Model, params, errorMessage}) {
		let actualParams = JSON.parse(JSON.stringify(params.filter(item => item.isGet || item.isGlobal)));
		actualParams.forEach(item => item.isRequest = item.isRequest || item.isGlobal && item.isRequest);

		runTime.api.add({
			name, path, preprocessor,
			params: actualParams,
			async handle(req) {
				let where = {};
				req.nc.config.params.filter(item => item.isGet).forEach(param => {
					if (req.nc.args[param.name] === undefined) return;
					if (param.mix && param.mixPrefix) {
						let mix = where[param.mix] || {};
						mix[param.mix][param.mixPrefix] = req.nc.args[param.name];
						return where[param.mix] = mix;
					}

					where[param.name] = req.nc.args[param.name];
				});
				let db_model = await Model.findOne({where});
				if (!db_model) throw errorMessage ? errorMessage : '不存在该对象';
				return db_model;
			}
		});
	},
	add({name, path, preprocessor, Model, params}) {
		let actualParams = JSON.parse(JSON.stringify(params.filter(item => item.isAdd || item.isGlobal || item.isBase)));
		runTime.api.add({
			name, path, preprocessor,
			params: actualParams,
			async handle(req) {
				return new Model(req.nc.args).save();
			}
		});
	},
	modify({name, path, preprocessor, Model, params, errorMessage}) {
		let actualParams = JSON.parse(JSON.stringify(params.filter(item => item.isModify || item.isGlobal || item.isBase)));
		actualParams.forEach(item => item.isRequest = item.isModifyKey || (item.isGlobal && item.isRequest));
		runTime.api.add({
			name, path, preprocessor,
			params: actualParams,
			async handle(req) {
				let where = {};
				req.nc.config.params.filter(item => item.isModifyKey).forEach(param => {
					if (req.nc.args[param.name] === undefined) return;
					if (param.mix && param.mixPrefix) {
						let mix = where[param.mix] || {};
						mix[param.mix][param.mixPrefix] = req.nc.args[param.name];
						return where[param.mix] = mix;
					}

					where[param.name] = req.nc.args[param.name];
				});
				let db_model = await Model.findOne({where});
				if (!db_model) throw errorMessage ? errorMessage : '不存在该对象';
				db_model.set(req.nc.args);
				return db_model.save();
			}
		});
	},
	delete({name, path, preprocessor, Model, params, errorMessage}) {
		let actualParams = JSON.parse(JSON.stringify(params.filter(item => item.isDelete || item.isGlobal)));
		actualParams.forEach(item => item.isRequest = item.isRequest || item.isGlobal && item.isRequest);

		runTime.api.add({
			name, path, preprocessor,
			params: actualParams,
			async handle(req) {
				let where = {};
				req.nc.config.params.filter(item => item.isDelete).forEach(param => {
					if (req.nc.args[param.name] === undefined) return;
					if (param.mix && param.mixPrefix) {
						let mix = where[param.mix] || {};
						mix[param.mix][param.mixPrefix] = req.nc.args[param.name];
						return where[param.mix] = mix;
					}

					where[param.name] = req.nc.args[param.name];
				});
				let db_model = await Model.findOne({where});
				if (!db_model) throw errorMessage ? errorMessage : '不存在该对象';
				await db_model.destroy();
			}
		});
	}
};
ExpressApi.quickUtil = {
	$like() {
		return {mixPrefix: '$like', mixValue: (item => `%${item}%`).toString()};
	}
};


module.exports = ExpressApi;

