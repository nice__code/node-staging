import dao from "./dao";

const axios = require('axios');
let host = '/'
let apiModel = {};
apiModel.setHost = h => host = h;

function stringToByte(str) {
	var bytes = new Array();
	var len, c;
	len = str.length;
	for (var i = 0; i < len; i++) {
		c = str.charCodeAt(i);
		if (c >= 0x010000 && c <= 0x10FFFF) {
			bytes.push(((c >> 18) & 0x07) | 0xF0);
			bytes.push(((c >> 12) & 0x3F) | 0x80);
			bytes.push(((c >> 6) & 0x3F) | 0x80);
			bytes.push((c & 0x3F) | 0x80);
		} else if (c >= 0x000800 && c <= 0x00FFFF) {
			bytes.push(((c >> 12) & 0x0F) | 0xE0);
			bytes.push(((c >> 6) & 0x3F) | 0x80);
			bytes.push((c & 0x3F) | 0x80);
		} else if (c >= 0x000080 && c <= 0x0007FF) {
			bytes.push(((c >> 6) & 0x1F) | 0xC0);
			bytes.push((c & 0x3F) | 0x80);
		} else {
			bytes.push(c & 0xFF);
		}
	}
	return bytes;
}

function Value2Bytes(str) {
	str = parseInt(str);
	str = str.toString(16);
	var pos = 0;
	var len = str.length;
	if (len % 2 != 0) {
		str = '0' + str;
	}
	len /= 2;
	var hexA = new Array();
	for (var i = 0; i < len; i++) {
		var s = str.substr(pos, 2);
		var v = parseInt(s, 16);
		hexA.push(v);
		pos += 2;
	}
	return hexA;
}

function a1(str) {
	let list = stringToByte(str);
	let ch = Date.now();
	let TimeArray = Value2Bytes(ch);

	let timeIndex1 = TimeArray[TimeArray.length - 1];
	let timeIndex2 = TimeArray[TimeArray.length - 2];
	let timeOther = TimeArray.slice(0, TimeArray.length - 2);
	let mask = timeIndex1 ^ timeIndex2;
	let random = (Math.random() * 256 | 1) % 256;
	let dataMaskSeed = (timeIndex1 + timeIndex2 + random) % 256;
	let dataMask = TimeArray.map(item => item ^ dataMaskSeed);

	let merge = [TimeArray.length ^ mask, random, timeIndex1 ^ random, timeIndex2 ^ random, ...list.map((item, index) => item ^ dataMask[index % dataMask.length]), ...timeOther.map(item => item ^ mask)];
	return String.fromCharCode(...merge);
};

//敏感字符串加密
let header = unescape('\\u0068\\u0065\\u0061\\u0064\\u0065\\u0072\\u0073'.replace(/\\u/g, "%u"))
let ContentType = unescape('\\u0043\\u006f\\u006e\\u0074\\u0065\\u006e\\u0074\\u002d\\u0054\\u0079\\u0070\\u0065'.replace(/\\u/g, "%u"))
let nicecodeA1 = unescape('\\u006e\\u0069\\u0063\\u0065\\u0063\\u006f\\u0064\\u0065\\u002f\\u0061\\u0031'.replace(/\\u/g, "%u"))
let secret = unescape('\\u002f\\u0073\\u0065\\u0063\\u0072\\u0065\\u0074'.replace(/\\u/g, "%u"))
let post = unescape('\\u0070\\u006f\\u0073\\u0074'.replace(/\\u/g, "%u"))
let apiConfig = {}
apiConfig[header] = {}
apiConfig[header][ContentType] = nicecodeA1
apiModel.runApi = async ({path, params, host = host}) => {
	let res = await axios[post](host + secret,
		a1(JSON.stringify({path, params})),apiConfig);


	if (res.data.code === 200) return res.data.data;
	throw res.data;
}
export default apiModel;