let db = require('../config/db/db');

let QuickData = {
	Type: {
		findOne: 'findOne',
		findAll: 'findAll',
		findAndCountAll: 'findAndCountAll',
		add: 'add',
		upsert: 'upsert',
		delete: 'delete',
		update: 'update',

	},
	preprocessor: [],
	async express(req, res) {
		let arg = Object.assign({}, req.params, req.query, req.body);

		//参数检验
		let model = db.Models[arg.model];
		if (!model) return res.jsonp({code: 501, message: '不存在该模型'});

		//前置检验
		try {
			await Promise.all(QuickData.preprocessor.map(func => Promise.resolve().then(() => func(arg))));
		} catch (e) {
			return res.jsonp({code: 501, message: '前置管理器拦截'});
		}

		//实际操作
		try {
			if (arg.type === QuickData.Type.findOne) return res.jsonp({code: 200, data: await model.findOne(arg)});
			if (arg.type === QuickData.Type.findAll) return res.jsonp({code: 200, data: await model.findAll(arg)});
			if (arg.type === QuickData.Type.findAndCountAll) return res.jsonp({
				code: 200,
				data: await model.findAndCountAll(arg)
			});
			if (arg.type === QuickData.Type.upsert) return res.jsonp({code: 200, data: await model.upsert(arg.values)});
			if (arg.type === QuickData.Type.add) return res.jsonp({code: 200, data: await model.create(arg.values)});
			if (arg.type === QuickData.Type.delete) return res.jsonp({code: 200, data: await model.destroy(arg)});
			if (arg.type === QuickData.Type.update) return res.jsonp({
				code: 200,
				data: await model.update(arg.values, arg)
			});

			return res.jsonp({code: 200, data: await model.findAndCountAll(arg)});
		} catch (e) {
			console.error(e);
			return res.jsonp({code: 500, message: '发生了尚未预知的错误'});
		}
	}
};
module.exports = QuickData;
