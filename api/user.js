let db = require('../config/db/db');
let define = require('../config/define');
let runTime = require('../config/runTime');
let userLogic = require('../logic/user');
let ExpressApi = require('../plugin/ExpressApi');


let {User} = db.Models;
let {api} = runTime;
let type = ExpressApi.ParamsType;


api.add({
	name: '用户登录',
	path: '/user/login',
	params: [
		{type: type.String, name: 'username', isRequest: true, comment: '用户名'},
		{type: type.String, name: 'password', isRequest: true, comment: '密码'},
	],
	group: 'user',
	async handle(req) {
		let {username, password} = req.nc.args;
		let db_user = await User.findOne({where: {username}});

		//登录信息验证
		if (!db_user) return Promise.reject('账号或密码错误');
		if (!db_user.enable) return Promise.reject('该账号暂时无法登陆');
		if (userLogic.decodePassword(db_user.salt, db_user.password) !== password) return Promise.reject('账号或密码错误');
		db_user.lastLoginTime = new Date();


		//登录信息记录
		await runTime.sessionClass.createSession({req, owner: db_user.id, ownerType: define.sessionType.USER});
		await db_user.save();
		return {
			username: db_user.username,
			lastLoginTime: db_user.lastLoginTime,
			permissions: db_user.permissions,
			token: req.nc.session.token
		};
	},

});









