let express = require('express');
let router = express.Router();

let path = require('path');
let fs = require('fs');

module.exports = router;

let apiPath = path.resolve(__dirname, '../api');

fs.readdirSync(apiPath).forEach(fileName => {
	//获取api目录下所有的文件(仅根目录),并加载
	let fileInfo = path.parse(path.resolve(apiPath, fileName));
	let file = fs.statSync(path.resolve(apiPath, fileName));
	if (file.isDirectory() || fileInfo.ext !== '.js') return;
	// console.log("路由", fileInfo.name, path.relative(__dirname, path.resolve(apiPath, fileName)))
	require(path.relative(__dirname, path.resolve(apiPath, fileName)));
});

