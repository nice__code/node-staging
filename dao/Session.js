const db = require('../config/db/db');
const define = require('../config/define');
const Sequelize = require('sequelize');
const moment = require('moment');

let modeConfig = {
	modelName: 'Session',
	name: 'nc_session',
	params: {
		id: {type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true},
		value: {
			type: Sequelize.JSON,
			defaultValue: {},
			comment: 'session的记录值,json格式,model读取时按照对象的格式'
		},
		expiration: {
			type: Sequelize.DATE,
			defaultValue() {
				return moment().add({seconds: define.sessionExpiration});
			},
			comment: '用于记录过期时间,过期时间在define中设定'
		}
	},
	tableConfig: {
		descName: 'Session记录表',
		comment: '[非逻辑表] 用于DBSession插件记录信息',

		scopes: {
			valid: {
				where: {
					expiration: {
						[db.Op.gte]: new Date()
					}
				}
			}
		},
		//默认开启 自动的createdAt,updatedAt
		timestamps: true,

		//索引
		indexes: [
			{fields: ['expiration']},
		]
	}
};


//生成model
const model = db.define(modeConfig.name, modeConfig.params, modeConfig.tableConfig);

//采用异步加载
let db_sync = model.sync().then(() => db.Models[modeConfig.modelName] = model);
db.sync(() => db_sync);
module.exports = model;

