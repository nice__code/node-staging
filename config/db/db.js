const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');
const Op = Sequelize.Op;
const operatorsAliases = {
	$eq: Op.eq,
	$ne: Op.ne,
	$gte: Op.gte,
	$gt: Op.gt,
	$lte: Op.lte,
	$lt: Op.lt,
	$not: Op.not,
	$in: Op.in,
	$notIn: Op.notIn,
	$is: Op.is,
	$like: Op.like,
	$notLike: Op.notLike,
	$iLike: Op.iLike,
	$notILike: Op.notILike,
	$regexp: Op.regexp,
	$notRegexp: Op.notRegexp,
	$iRegexp: Op.iRegexp,
	$notIRegexp: Op.notIRegexp,
	$between: Op.between,
	$notBetween: Op.notBetween,
	$overlap: Op.overlap,
	$contains: Op.contains,
	$contained: Op.contained,
	$adjacent: Op.adjacent,
	$strictLeft: Op.strictLeft,
	$strictRight: Op.strictRight,
	$noExtendRight: Op.noExtendRight,
	$noExtendLeft: Op.noExtendLeft,
	$and: Op.and,
	$or: Op.or,
	$any: Op.any,
	$all: Op.all,
	$values: Op.values,
	$col: Op.col
};
//根据预设项,默认数据库连接
const config = require('../index');
const db = new Sequelize(`${config.db.type}://${config.db.username}:${config.db.password}@${config.db.url}/${config.db.database}`, {
	operatorsAliases,
	logging: () => {
	},
	'pool': {
		'max': 10,
		'min': 1,
		'idle': 200000,
		'acquire': 200000
	},
});

//数据库加载顺序链 (用于判断所有)
let sync_db = Promise.resolve();

//数据库模块集合
db.Models = {};

//辅助查询工具
db.util = {
	//子查询辅助
	subSelect: (model, query) => {
		let sql = db.dialect.QueryGenerator.selectQuery(model.tableName, query);
		sql = sql.replace(';', '');
		return db.literal(`( ${sql} )`);
	},

};

//数据库同步函数 - 链式加载
db.sync = (taskFunc = () => Promise.resolve()) => sync_db = sync_db.then(() => taskFunc());

module.exports = db;


//读取所有的dao
let daoPath = path.resolve(__dirname, '../../dao');
fs.readdirSync(daoPath).forEach(fileName => {
	//获取api目录下所有的文件(仅根目录),并加载
	let fileInfo = path.parse(path.resolve(daoPath, fileName));
	let file = fs.statSync(path.resolve(daoPath, fileName));
	if (file.isDirectory() || fileInfo.ext !== '.js') return;
	require(path.relative(__dirname, path.resolve(daoPath, fileName)));
});
