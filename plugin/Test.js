const _ = require('lodash');
const define = require('../config/define');
let test = {};

test.runTest = async ({args, index}) => {


	let test_instance = args && args.test && args.test[index];
	if (!test_instance) throw '不存在该测试';

	let result = {
		pass: true,   //测试结果
		testReturn: null,   //测试运行返回值
		testCheck: null,   //测试运行验证是否成功
	};


	let data = await define.api.runApi({path: args.path.toString(), params: test_instance.params});
	result.testReturn = data;

	if (test_instance.value) result.pass = result.pass && _.isEqual(data, test_instance.value);
	if (test_instance.resultCheck) result.pass = result.pass && await test_instance.resultCheck(data);

	return result;

};

module.exports = test;

