const redis = require('redis');
const moment = require('moment');
const util = require('util');
const config = require('./index');
let define = require('../config/define');


if (!config.redis) return module.exports = {};

let client = redis.createClient({
	host: config.redis.url,
	port: config.redis.port,
	password: config.redis.password,
	db: config.redis.dbIndex
});

client.async = {
	set: util.promisify(client.set).bind(client),
	setex: util.promisify(client.setex).bind(client),
	mget: util.promisify(client.mget).bind(client),
	get: util.promisify(client.get).bind(client),
	del: util.promisify(client.del).bind(client),
	hkeys: util.promisify(client.hkeys).bind(client),
	hget: util.promisify(client.hget).bind(client),
	hmget: util.promisify(client.hmget).bind(client),
	hvals: util.promisify(client.hvals).bind(client),
	hset: util.promisify(client.hset).bind(client),
	hdel: util.promisify(client.hdel).bind(client),
	expire: util.promisify(client.expire).bind(client),
	expireat: util.promisify(client.expireat).bind(client),

	incr: util.promisify(client.incr).bind(client),
}


client.nc = {};

client.nc.setSession = async ({session}) => client.async.setex(`nc_session:${session.token}`, define.sessionExpiration, JSON.stringify(session.toJSON()))
client.nc.getSession = async ({token}) => client.async.get(`nc_session:${token}`);
client.nc.deleteSession = async ({token}) => client.async.del(`nc_session:${token}`);

//访问时错误数量+1, 并如果超过特定数量时,自动增加禁止列表
client.nc.apiErrorCountIncrease = async ({ip}) => {
	let data = await client.async.incr(`api_error:${ip}`);
	if (data === 1) await client.async.expire(`api_error:${ip}`, define.apiInterceptForErrorLimit_recordTime);
	if (data > define.apiInterceptForErrorLimit_number) {
		client.nc.ipBanInTime({ip, time: define.apiInterceptForErrorLimit_banTime});
	}
	return data;
};

//禁止ip访问api Time秒
client.nc.ipBanInTime = async ({ip, time}) => client.async.setex(`ip_ban:${ip}`, time, Date.now());

//判断ip是否被禁止了
client.nc.isIpBan = async ({ip}) => client.async.get(`ip_ban:${ip}`);

module.exports = client;