let crypto = require('crypto');


let db = require('../config/db/db');
let define = require('../config/define');
let ExpressApi = require('../plugin/ExpressApi');
let { User} = db.Models;
let logic = {};
module.exports = logic;

//密码加密函数
logic.encodePassword = function (salt, password) {
	const hmac = crypto.createHmac('md5', salt);
	hmac.update(define.adminSalt);
	let key = hmac.digest('hex');

	const passwordCipher = crypto.createCipheriv('aes256', key, define.adminIV);
	let encodePass = passwordCipher.update(password, 'utf8', 'hex');
	encodePass += passwordCipher.final('hex');

	return encodePass;
};

//密码解密函数
logic.decodePassword = function (salt, encrypted) {
	const hmac = crypto.createHmac('md5', salt);
	hmac.update(define.adminSalt);
	let key = hmac.digest('hex');

	const passwordCipher = crypto.createDecipheriv('aes256', key, define.adminIV);
	let encodePass = passwordCipher.update(encrypted, 'hex', 'utf8');
	encodePass += passwordCipher.final('utf8');

	return encodePass;
};
logic.preprocessor_setUserFromSession = async (req) => {
	let nc = req.nc;
	if (nc.session && nc.session.owner && nc.session.ownerType === define.sessionType.USER)
		req.nc.db_user = await User.findOne({where: {id: nc.session.owner}});
	else throw ExpressApi.Error.authError();

};


