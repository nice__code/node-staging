const db = require('../config/db/db');
const Sequelize = require('sequelize');


let modeConfig = {
	modelName: 'AdminAndRole',
	name: 'admin_with_role',
	params: {
		id: {type: Sequelize.BIGINT.UNSIGNED, autoIncrement: true, primaryKey: true},
		adminId: {type: Sequelize.BIGINT.UNSIGNED, comment: '属于的管理员(管理员ID)'},
		roleId: {type: Sequelize.BIGINT.UNSIGNED, comment: '拥有的角色(角色ID)'},
	},
	tableConfig: {
		descName: '管理员的角色表',
		comment: '用于这个管理员有那种角色',
		//默认开启 自动的createdAt,updatedAt
		timestamps: true,

		//索引
		indexes: [
			{fields: ['adminId']},
			{fields: ['roleId']},
		]
	}
};


//生成model
const model = db.define(modeConfig.name, modeConfig.params, modeConfig.tableConfig);

//采用异步加载
let db_sync = model.sync().then(() => db.Models[modeConfig.modelName] = model);
db.sync(() => db_sync);

module.exports = model;